USE alertasprev
go
/* **********************************************************************************************************************/
/* Nombre SP                     : sp_alerta_calc_hist                                                                  */
/* Nombre BD                     : alertasprev                                                                          */
/* Tipo de ejecucion             : Batch                                                                                */ 
/* Fecha creacion                : 14/10/2014                                                                           */
/* Fecha Modificado				 : 19/08/2016                                                                           */
/* Autor                         : Gabriel Martinez. (ADA LTDA)                                                         */
/* Modificado					 : Francisco Valenzuela                                                                 */
/* Objetivos                     : Proceso que realiza el calculo de las alertas de preventivas para mas de un periodo  */
/* Canal de ejecucion            : Control -M                                                                           */
/* Paramtro entrada              : NO aplica                                                                            */
/* Retorno                       : Datos en tablas fisicas                                                              */
/* Ejemplo de ejecucion          : exec sp_alerta_calc_hist                                                             */
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 19/08/2016                                                                           */
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se agrega y eliminan indicadores al porceso que realiza el calculo de las alertas 	*/
/* 									de preventivas para mas de un periodo												*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 06/09/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se agregan indicadores de Leverage Financiero 										*/
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 27/12/2016  																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se modifica los montos de balance, para que estos se encuentren proyectados, y cambio*/ 
/*									en formula del indicador 19															*/	
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 05/08/2021 																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se realiza un distinct de informacion al utiliazr la tabla APR_HIST_COMPLEMENTO 		*/
/*									ya que esta ahora es a nivel de operacion y antes era a nivel de clientes			*/ 
/* **********************************************************************************************************************/
/* Fecha Modificado				 : 27/12/2021 																			*/
/* Modificado					 : Francisco Valenzuela. (ADA LTDA)                                                     */
/* Objetivos                     : Se realiza un distinct de informacion al utiliazr la tabla APR_HIST_COMPLEMENTO 		*/
/*                                 para alertas 67, 70, 72, 76, 76, ya que esta ahora es a nivel de operacion 			*/
/*									y antes era a nivel de clientes														*/ 
/* **********************************************************************************************************************/

IF OBJECT_ID('sp_alerta_calc_hist') IS NOT NULL
BEGIN
    DROP PROCEDURE sp_alerta_calc_hist
    IF OBJECT_ID('sp_alerta_calc_hist') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE sp_alerta_calc_hist >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE sp_alerta_calc_hist >>>'
END
go
CREATE PROCEDURE sp_alerta_calc_hist
AS
BEGIN
Print 'Inicio Procedimiento sp_alerta_calc_hist'

SET CHAINED OFF

Print 'Crea tabla temporal #banca_cliente'
CREATE TABLE #banca_cliente
(
	 RUT				int 	 		not null		
	,PERIODO_PROC		decimal(16,0)	not null
	,CLI_COD_BANCA		char(3)			null
	,PAR_CODIGO			char(20)		null
	,FEC_EJEC			datetime		null	

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #banca_cliente'
				RETURN -1
	END


Print 'Inserta Agrupacion de banca de clientes en la tabla #banca_cliente'
INSERT INTO #banca_cliente
SELECT 
	 a.RUT
	,a.PERIODO_PROC
	,a.CLI_COD_BANCA
	,b.PAR_CODIGO
	,a.FEC_EJEC
FROM 
        APR_HIST_CLIENTE a
LEFT JOIN 
		APR_PARAMETRO b
ON
    a.CLI_COD_BANCA = b.PAR_ADICIONAL
AND b.PAR_VIGENCIA = 'S' 
AND b.PAR_CONCEPTO = 'AGRBANCAS'
WHERE
	a.PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')


	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto -->  Insert a tabla #banca_cliente'
		return -1
	END


	CREATE UNIQUE CLUSTERED INDEX x_banca_cliente1
    ON #banca_cliente(RUT,PERIODO_PROC)
	
	CREATE INDEX x_banca_cliente1_2
    ON #banca_cliente(RUT)
	

/* *********************************************************** */

Print 'Crea tabla temporal #complemento_act'
CREATE TABLE #complemento_act
(
	 RUT				int 	 		not null		
	,PERIODO_PROC		decimal(16,0)	not null		
	,CPL_VAL_PI			decimal (18,6)	null
	,CPL_SDO_PROM_MES	decimal (18,4)	null
	,CPL_MTO_VTA_MES	decimal (18,4)	null	
	,CPL_MTO_GAR_GEN		decimal (18,4)	null
	,CPL_CANT_PRG_TOT	smallint		null
	,CPL_CANT_REN		smallint		null
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #complemento_act'
				RETURN -1
	END


Print 'Insert a tabla #complemento_act'
INSERT INTO #complemento_act
SELECT DISTINCT
     RUT
	,PERIODO_PROC
	,CPL_VAL_PI
	,CPL_SDO_PROM_MES
	,CPL_MTO_VTA_MES
	,CPL_MTO_GAR_GEN
	,CPL_CANT_PRG_TOT
	,CPL_CANT_REN		
FROM APR_HIST_COMPLEMENTO 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')

	
	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto -->  Insert a tabla #complemento_act'
		return -1
	END
	
	CREATE UNIQUE CLUSTERED INDEX x_complemento_act1
    ON #complemento_act(RUT,PERIODO_PROC)	
	
	CREATE INDEX x_complemento_act1_2
    ON #complemento_act(RUT)
	
Print 'Crea tabla temporal #complemento_ant'
CREATE TABLE #complemento_ant
(
	 RUT				int 	 		not null		
	,PERIODO_PROC		decimal(16,0)	not null		
	,CPL_VAL_PI			decimal (18,6)	null
	,CPL_SDO_PROM_MES	decimal (18,4)	null		--mes anterior
	,CPL_MTO_VTA_MES	decimal (18,4)	null	
	,CPL_MTO_GAR_GEN		decimal (18,4)	null		--mes anterior
	,CPL_CANT_PRG_TOT	smallint		null
	,CPL_CANT_REN		smallint		null

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #complemento_ant'
				RETURN -1
	END


Print '#complemento_act #complemento_ant'
INSERT INTO #complemento_ant
SELECT DISTINCT
	 RUT
	,PERIODO_PROC
	,CPL_VAL_PI
	,CPL_SDO_PROM_MES
	,CPL_MTO_VTA_MES
	,CPL_MTO_GAR_GEN
	,CPL_CANT_PRG_TOT
	,CPL_CANT_REN		
FROM APR_HIST_COMPLEMENTO 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOANT' and PAR_VIGENCIA = 'S')	


	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto -->  Insert a tabla #complemento_ant'
		return -1
	END	

	CREATE UNIQUE CLUSTERED INDEX x_complemento_ant1
    ON #complemento_ant(RUT,PERIODO_PROC)
	
	CREATE INDEX x_complemento_ant1_2
    ON #complemento_ant(RUT)

/* ************************** CALCULOS *********************************** */	

Print 'Crea tabla temporal #tabla_indicador'
CREATE TABLE #tabla_indicador
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #tabla_indicador'
				RETURN -1
	END	

	CREATE UNIQUE INDEX x_tabla_indicador1
    ON #tabla_indicador(RUT,COD_INDICADOR)
	
	CREATE INDEX x_tabla_indicador2
    ON #tabla_indicador(RUT)
	
	CREATE INDEX x_tabla_indicador3
    ON #tabla_indicador(ESTADO)

Print 'Inserta calificaciones a tabla temporal #tabla_indicador'
/* ********** Variable: Disminucion en los saldos promedios Cct ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,71
	,CASE 	WHEN ISNULL(b.CPL_SDO_PROM_MES,0) = 0 AND ISNULL(a.CPL_SDO_PROM_MES,0) = 0 THEN 'N'	
			WHEN (a.CPL_SDO_PROM_MES <= (b.CPL_SDO_PROM_MES * (CONVERT(DECIMAL(4,2),c.PAR_ADICIONAL)))) THEN 'S' ELSE 'N' END as sdo_prom_mes --este valor tiene que ser parametrico
FROM 
	 #complemento_act a
LEFT JOIN
	 #complemento_ant b
ON 
	a.RUT = b.RUT
LEFT JOIN
    APR_PARAMETRO c
ON
	c.PAR_CONCEPTO = 'INDICADORES' 
AND c.PAR_CODIGO = '71' 
AND c.PAR_VIGENCIA = 'S'    
	
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Disminucion en los saldos promedios Cct'
			return -1
		END

	 
/* ********** Variable: Prorrogas de los montos totales ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,67
    ,CASE 
        WHEN max (b.CPL_CANT_PRG_TOT) > ((SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '67')) 
            THEN 'S' ELSE 'N' END
FROM 
    #complemento_act a
LEFT JOIN
	 (SELECT DISTINCT RUT, CPL_CANT_PRG_TOT,PERIODO_PROC FROM APR_HIST_COMPLEMENTO) b
ON
    a.RUT = b.RUT
AND	b.PERIODO_PROC >= (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S')	
GROUP BY a.RUT
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Prorrogas de los montos totales'
			return -1
		END
		
		
		
/* ********** Variable: Renovaciones que implican modificaciones en el plan de pago ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,70
    ,CASE 
        WHEN max (b.CPL_CANT_REN) > ((SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '70')) 
            THEN 'S' ELSE 'N' END
FROM 
    #complemento_act a
LEFT JOIN
	 (SELECT DISTINCT RUT, CPL_CANT_REN,PERIODO_PROC FROM APR_HIST_COMPLEMENTO) b
ON
    a.RUT = b.RUT
AND	b.PERIODO_PROC >= (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S')	
GROUP BY a.RUT
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Renovaciones que implican modificaciones en el plan de pago'
			return -1
		END		
		

/* ********** Variable PI: Diferencia entre PI actual y la PI de seis meses atras es mayor a 5% ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,76
    ,CASE 	WHEN b.RUT IS NULL THEN 'N'	
			WHEN (a.CPL_VAL_PI - b.CPL_VAL_PI) > ((SELECT (CONVERT(decimal(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '76')) 
                THEN 'S' ELSE 'N' END 
FROM 
    #complemento_act a
LEFT JOIN
    (SELECT DISTINCT RUT, CPL_VAL_PI,PERIODO_PROC FROM APR_HIST_COMPLEMENTO) b
ON 
    a.RUT = b.RUT
AND b.PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S') -- estas fechas tienen que estar por parametro 		

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Prorrogas de los montos totales'
			return -1
		END				
		
		
/* ********** Variable PI: Aumento de la PI en 20% respecto a seis meses atras ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,75
    ,CASE 	WHEN b.RUT IS NULL THEN 'N'	
			    WHEN a.CPL_VAL_PI > (b.CPL_VAL_PI * ((SELECT (CONVERT(decimal(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '75')) ) 
                    THEN 'S' ELSE 'N' END
FROM 
    #complemento_act a
LEFT JOIN
    (SELECT DISTINCT RUT, CPL_VAL_PI,PERIODO_PROC FROM APR_HIST_COMPLEMENTO) b
ON 
    a.RUT = b.RUT
AND b.PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S') -- estas fechas tienen que estar por parametro 		
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Prorrogas de los montos totales'
			return -1
		END			
		


/* ********** Variable: Ventas mensuales (IVA) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,72
    ,CASE 	WHEN b.RUT IS NULL THEN 'N'	
	          WHEN b.CPL_MTO_VTA_MES = 0 THEN 'N'
			    WHEN a.CPL_MTO_VTA_MES <= (b.CPL_MTO_VTA_MES * ((SELECT (CONVERT(decimal(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '72')) ) 
                    THEN 'S' ELSE 'N' END
FROM 
    #complemento_act a
LEFT JOIN
    (SELECT DISTINCT RUT, CPL_MTO_VTA_MES,PERIODO_PROC FROM APR_HIST_COMPLEMENTO) b
ON 
    a.RUT = b.RUT
AND b.PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S') -- estas fechas tienen que estar por parametro 		
			

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Prorrogas de los montos totales'
			return -1
		END			
		

		
  /* *****************************************************/
 /* Calculo Garantia                                    */
/* *****************************************************/	

Print 'Crea tabla temporal #deudabci_act'
CREATE TABLE #deudabci_act
(
	 RUT				    int 	 		not null		
	,PERIODO_PROC			decimal(16,0)	not null		
	,BCI_MTO_DEU_DIR_BCI	decimal (18,4)	null
	,BCI_DEU_MOR_16_30		decimal (18,4)	null
	,BCI_IND_USO_80_LSB		char(1)      	null

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudabci_act'
				RETURN -1
	END	

	
Print 'Insert a tabla #deudabci_act'
INSERT INTO #deudabci_act
SELECT 
	 RUT
	,PERIODO_PROC
	,BCI_MTO_DEU_DIR_BCI
	,BCI_DEU_MOR_16_30
	,BCI_IND_USO_80_LSB
FROM APR_HIST_DEUDABCI
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')	


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #deudabci_act'
			return -1
		END			

	CREATE UNIQUE CLUSTERED INDEX x_deudabci_act1
    ON #deudabci_act(RUT,PERIODO_PROC)
	
	CREATE INDEX x_deudabci_act1_2
    ON #deudabci_act(RUT)
	
/* ********************************************************* */	
Print 'Crea tabla temporal #deudabci_ant'
CREATE TABLE #deudabci_ant
(
	 RUT				    int 	 		not null		
	,PERIODO_PROC			decimal(16,0)	not null		
	,BCI_MTO_DEU_DIR_BCI	decimal (18,4)	null
	,BCI_IND_USO_80_LSB		char(1)      	null
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudabci_ant'
				RETURN -1
	END	

	
Print 'Insert a tabla #deudabci_ant'
INSERT INTO #deudabci_ant
SELECT 
	 RUT
	,PERIODO_PROC
	,BCI_MTO_DEU_DIR_BCI
	,BCI_IND_USO_80_LSB
FROM APR_HIST_DEUDABCI
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOANT' and PAR_VIGENCIA = 'S')	

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #deudabci_ant'
			return -1
		END			

	CREATE UNIQUE CLUSTERED INDEX x_deudabci_ant1
    ON #deudabci_ant(RUT,PERIODO_PROC)
	
	CREATE INDEX x_deudabci_ant1_2
    ON #deudabci_ant(RUT)
		
/* ********************************************************* */
	

Print 'Crea tabla temporal #garantia_act'
CREATE TABLE #garantia_act
(
	 RUT				int 	 		not null		
	,MTO_GAR_ACT		decimal (18,6)	null		--mes anterior

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #garantia_act'
				RETURN -1
	END	



Print '--------------------------------'
INSERT INTO #garantia_act
SELECT 
    a.RUT
    ,CASE WHEN a.CPL_MTO_GAR_GEN = 0 THEN 0 ELSE (convert(decimal(18,4), (b.BCI_MTO_DEU_DIR_BCI / a.CPL_MTO_GAR_GEN))) END 
from 
    #complemento_act a
    ,#deudabci_act b
where
    a.RUT = b.RUT

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #garantia_act'
			return -1
		END			

	CREATE UNIQUE CLUSTERED INDEX x_garantia_act1
    ON #garantia_act(RUT)
	
Print 'Crea tabla temporal #garantia_ant'
CREATE TABLE #garantia_ant
(
	 RUT				int 	 		not null		
	,MTO_GAR_ANT		decimal (18,6)	null		--mes anterior
)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #garantia_ant'
				RETURN -1
	END	

	
Print '--------------------------------'
INSERT INTO #garantia_ant
select 
    a.RUT
    ,CASE WHEN a.CPL_MTO_GAR_GEN = 0 THEN 0 ELSE (convert(decimal(18,4), (b.BCI_MTO_DEU_DIR_BCI / a.CPL_MTO_GAR_GEN))) END 
from 
    #complemento_ant a
    ,#deudabci_ant b
where
    a.RUT = b.RUT

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #garantia_ant'
			return -1
		END			
	
	CREATE UNIQUE CLUSTERED INDEX x_garantia_ant1
    ON #garantia_ant(RUT)
	
/* ********* Variable: Garantia ************ */
INSERT INTO #tabla_indicador
SELECT 
    a.RUT
	,73
	,CASE 	WHEN b.MTO_GAR_ANT = 0 THEN 'N'	    
			    WHEN (a.MTO_GAR_ACT <= (b.MTO_GAR_ANT * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '73'))) 
                    THEN 'S' ELSE 'N' END
FROM 
    #garantia_act a
LEFT JOIN     
    #garantia_ant b
ON
    a.RUT = b.RUT


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Garantia'
			return -1
		END			
	


  /* *****************************************************/
 /* Calculo APR_HIST_DEUDABCI                                    */
/* *****************************************************/		
--Duda	
INSERT INTO #tabla_indicador
SELECT 
     a.RUT
    ,68
    ,CASE   WHEN b.BCI_IND_USO_80_LSB IS NULL THEN 'N'
            WHEN a.BCI_IND_USO_80_LSB = 'S' AND b.BCI_IND_USO_80_LSB = 'N' THEN 'S' ELSE 'N' END
FROM 
    #deudabci_act a
LEFT JOIN
	#deudabci_ant b
ON
    a.RUT = b.RUT


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Lineas de sobregiro permanentemente copadas'
			return -1
		END


		
Print 'Crea tabla temporal #deudabci_ult6'
CREATE TABLE #deudabci_ult6
(
     PERIODO_PROC       decimal(16,0)   not null
	,RUT				int 	 		not null		
	,BCI_POR_USO_LSB	decimal(18,4)   null

)		
		
	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudabci_ult6'
				RETURN -1
	END			


	
INSERT INTO #deudabci_ult6
SELECT 
    PERIODO_PROC       
    ,RUT			
    ,BCI_POR_USO_LSB
FROM 
    APR_HIST_DEUDABCI
WHERE 
    PERIODO_PROC >= (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S') 
		
		
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #deudabci_ult6'
			return -1
		END
		
	CREATE UNIQUE CLUSTERED INDEX x_deudabci_ult61
    ON #deudabci_ult6(PERIODO_PROC,RUT)
	
	CREATE INDEX x_deudabci_ult61_2
    ON #deudabci_ult6(PERIODO_PROC)

Print 'Crea tabla temporal #nro_cli_ult6'
CREATE TABLE #nro_cli_ult6
(
	 RUT				int 	 		not null		
	,nro_cli        	int             null

)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #nro_cli_ult6'
				RETURN -1
	END	
	


INSERT INTO #nro_cli_ult6
select 
    a.RUT
    ,SUM(CASE WHEN b.CORRELATIVO in (1,2,3,4,5,6) and a.BCI_POR_USO_LSB > (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '69') 
            THEN 1 ELSE 0 END) 
from 
    #deudabci_ult6 a
    ,APR_FECHA_PROCESO b
where
    a.PERIODO_PROC = b.PERIODO_PROC
group by a.RUT		
		
		
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #nro_cli_ult6'
			return -1
		END

	CREATE UNIQUE CLUSTERED INDEX x_nro_cli_ult61
    ON #nro_cli_ult6(RUT)

INSERT INTO #tabla_indicador
SELECT 
	 RUT
	,69
    ,CASE WHEN nro_cli > 5 THEN 'S' ELSE 'N' END
FROM 
    #nro_cli_ult6


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Lineas de sobregiro permanentemente copadas'
			return -1
		END
			
	
		
/* ********** Variable: Mora de 16-30 dias BCI ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,51
    ,CASE WHEN max (b.BCI_DEU_MOR_16_30) > (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '51')
			THEN 'S' ELSE 'N' END
FROM 
    #deudabci_act a
LEFT JOIN
	 APR_HIST_DEUDABCI b
ON
    a.RUT = b.RUT
AND	b.PERIODO_PROC >= (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO4ANT' and PAR_VIGENCIA = 'S')	
GROUP BY a.RUT


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Mora de 16-30 dias BCI'
			return -1
		END


	
/* **************************************** */
/*      		APR_BALANCE   					*/
/* **************************************** */
Print 'Crea tabla temporal #balance_act'
CREATE TABLE #balance_act
(
	 RUT			 integer
	,BIF_IND_PER	 char(3)
	,BIF_TIP_BAL	 smallint
	,BIF_FEC_BAL	 datetime
	,TIPO_BAL_DESC	 char(50)
    ,BIF_MTO_ROP     decimal(18,6) 
	,BIF_MTO_PAT	 decimal(18,6)
	,BIF_MTO_UTL	 decimal(18,6)
    ,BIF_MTO_EBITDA  decimal(18,6) 
    ,BIF_CTA_CBR     decimal(18,6) 
    ,BIF_PER_INV     integer
	,BIF_PER_CRD	 integer
	,BIF_IND_LVG	 decimal(18,4)	
    ,BIF_PAS_EXG     decimal(18,6) 
    ,BIF_VTA_ANU     decimal(18,6) 
    ,BIF_CTA_CBR_ER  decimal(18,6) 
    ,BIF_INV_ER      decimal(18,6) 
    ,BIF_TOT_ACT     decimal(18,6) 
    ,BIF_DEU_FIN     decimal(18,6)
    ,BIF_IND_LVG_FIN decimal(18,6) 
	,BIF_GTO_FIN	 decimal(18,6)
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #balance_act'
				RETURN -1
	END



Print 'Insert a tabla #balance_act datos de APR_BALANCE'	
INSERT INTO #balance_act
SELECT 
	 RUT
	,BIF_IND_PER	 
	,BIF_TIP_BAL	 
	,BIF_FEC_BAL	 
	,TIPO_BAL_DESC
	,convert(decimal(18,4),(BIF_MTO_ROP / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_ROP	
	,BIF_MTO_PAT
    ,convert(decimal(18,4),(BIF_MTO_UTL / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_UTL
    ,convert(decimal(18,4),(BIF_MTO_EBITDA / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_MTO_EBITDA
	,BIF_CTA_CBR   
	,BIF_PER_INV   
	,BIF_PER_CRD	
	,BIF_IND_LVG	
	,BIF_PAS_EXG   
    ,convert(decimal(18,4),(BIF_VTA_ANU / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_VTA_ANU	
	,BIF_CTA_CBR_ER
	,BIF_INV_ER   
    ,BIF_TOT_ACT     	
    ,convert(decimal(18,4),(BIF_DEU_FIN / (convert(int,(substring(( convert(varchar, BIF_FEC_BAL, 112)),5,2))))) * 12) as BIF_DEU_FIN	
    ,BIF_IND_LVG_FIN
	,BIF_GTO_FIN	
FROM 
	APR_BALANCE
WHERE 
    BIF_IND_PER = 'ACT'
	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #balance_act'
			return -1
		END	

	CREATE UNIQUE CLUSTERED INDEX x_balance_act1
    ON #balance_act(RUT,BIF_IND_PER,BIF_TIP_BAL,BIF_FEC_BAL)
		
	CREATE INDEX x_balance_act1_2
    ON #balance_act(RUT)
		
Print 'Crea tabla temporal #balance_ant'
CREATE TABLE #balance_ant
(
	 RUT			 integer
	,BIF_IND_PER	 char(3)
	,BIF_TIP_BAL	 smallint
	,BIF_FEC_BAL	 datetime
	,TIPO_BAL_DESC	 char(50)
    ,BIF_MTO_ROP     decimal(18,6) 
	,BIF_MTO_PAT	 decimal(18,6)
	,BIF_MTO_UTL	 decimal(18,6)
    ,BIF_MTO_EBITDA  decimal(18,6) 
    ,BIF_CTA_CBR     decimal(18,6) 
    ,BIF_PER_INV     integer
	,BIF_PER_CRD	 integer
	,BIF_IND_LVG	 decimal(18,4)	
    ,BIF_PAS_EXG     decimal(18,6) 
    ,BIF_VTA_ANU     decimal(18,6) 
    ,BIF_CTA_CBR_ER  decimal(18,6) 
    ,BIF_INV_ER      decimal(18,6) 
    ,BIF_IND_LVG_FIN     decimal(18,6) 
)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #balance_ant'
				RETURN -1
	END



Print 'Insert a tabla #balance_ant datos de APR_BALANCE'	
INSERT INTO #balance_ant
SELECT 
	 RUT
	,BIF_IND_PER	 
	,BIF_TIP_BAL	 
	,BIF_FEC_BAL	 
	,TIPO_BAL_DESC
	,BIF_MTO_ROP   
	,BIF_MTO_PAT
	,BIF_MTO_UTL
	,BIF_MTO_EBITDA	
	,BIF_CTA_CBR   
	,BIF_PER_INV   
	,BIF_PER_CRD	
	,BIF_IND_LVG	
	,BIF_PAS_EXG   
	,BIF_VTA_ANU   
	,BIF_CTA_CBR_ER
	,BIF_INV_ER        
	,BIF_IND_LVG_FIN	
FROM 
	APR_BALANCE
WHERE 
    BIF_IND_PER = 'ANT'
	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #balance_ant'
			return -1
		END	

	CREATE UNIQUE CLUSTERED INDEX x_balance_ant1
    ON #balance_ant(RUT,BIF_IND_PER,BIF_TIP_BAL,BIF_FEC_BAL)
	
	CREATE INDEX x_balance_ant1_2
    ON #balance_ant(RUT)


/* ********** Variable: Rop negativo ultimos 2 periodos ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,2
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_ROP,0) = 0 AND ISNULL(a.BIF_MTO_ROP,0) = 0 THEN 'N'	
			WHEN a.BIF_MTO_ROP < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '2.1')
                AND b.BIF_MTO_ROP < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '2.2')
                    THEN 'S' ELSE 'N' END 
FROM 
	#balance_act a
LEFT JOIN
	#balance_ant b
ON 
    a.RUT = b.RUT


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Rop negativo ultimo periodo'
			return -1
		END		
	
	

/* ********** Variable: Resultado Operacional ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,3
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_ROP,0) = 0 AND ISNULL(a.BIF_MTO_ROP,0) = 0 THEN 'N'	
			WHEN (a.BIF_MTO_ROP <= (b.BIF_MTO_ROP * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '3'))) 
                THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Resultado Operacional'
			return -1
		END

	

/* ********** Variable: Patrimonio ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,5
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_PAT,0) = 0 AND ISNULL(a.BIF_MTO_PAT,0) = 0 THEN 'N'	
				WHEN (a.BIF_MTO_PAT <= (b.BIF_MTO_PAT * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '5'))) 
					THEN 'S' ELSE 'N' END --este valor tiene que ser parametrico
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Patrimonio'
			return -1
		END

		
/* ************** Variable: Utilidad en el ultimo periodo > 0 ********************* */		

INSERT INTO #tabla_indicador
SELECT 
	 RUT
	,93
	,CASE 	WHEN BIF_MTO_UTL IS NULL THEN 'N'	
			WHEN BIF_MTO_UTL >  (CONVERT(INT,b.PAR_ADICIONAL)) THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act 
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '93' 
    and b.PAR_VIGENCIA = 'S' 

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Utilidad, Utilidad en el ultimo periodo > 0 > 0 '
			return -1
		END


/* ********** Variable: Utilidad, Perdida ultimos 2 periodos ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,7
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_UTL,0) = 0 AND ISNULL(a.BIF_MTO_UTL,0) = 0 THEN 'N'	
			    WHEN a.BIF_MTO_UTL < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '7.1') 
                    AND b.BIF_MTO_UTL < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '7.2') 
                        THEN 'S' ELSE 'N' END 
FROM 
	#balance_act a
LEFT JOIN
	#balance_ant b
ON 
    a.RUT = b.RUT

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Rop negativo ultimos dos periodos'
			return -1
		END	


		
		

/* ********** Variable: Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,10
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_EBITDA,0) = 0 AND ISNULL(a.BIF_MTO_EBITDA,0) = 0 THEN 'N'	
			    WHEN a.BIF_MTO_EBITDA < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '10.1') 
                    AND b.BIF_MTO_EBITDA < (SELECT (CONVERT(INT,PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '10.2') 
                        THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ebitda'
			return -1
		END
		

		
/* ********** Variable: Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,11
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_EBITDA,0) = 0 AND ISNULL(a.BIF_MTO_EBITDA,0) = 0 THEN 'N'	
			    WHEN (a.BIF_MTO_EBITDA <= (b.BIF_MTO_EBITDA * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '11'))) 
                    THEN 'S' ELSE 'N' END
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ebitda'
			return -1
		END


/* ********** Variable: Gastos Financieros sobre Ebitda ********** */
INSERT INTO #tabla_indicador
SELECT 
	 RUT
	,29
    ,case when a.BIF_MTO_EBITDA <> 0 then  
            case when (a.BIF_GTO_FIN / a.BIF_MTO_EBITDA) > (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
     end     

FROM
	 #balance_act a
     ,APR_PARAMETRO b 
WHERE
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '29' 
    and b.PAR_VIGENCIA = 'S'  

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END 
		
		
/* ********** Variable: Ebitda ********** */
/* ********** NUEVA (modifica anterior)  Disminucion  del Ebitda en 15 40% c/r al periodo anterior **********/
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,91
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_MTO_EBITDA,0) = 0 AND ISNULL(a.BIF_MTO_EBITDA,0) = 0 THEN 'N'	
			    WHEN (a.BIF_MTO_EBITDA <= (b.BIF_MTO_EBITDA * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '91'))) 
                    THEN 'S' ELSE 'N' END
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ebitda'
			return -1
		END


 /* ********** Variable: Cuentas Por Cobrar ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,12
	,case when a.BIF_CTA_CBR > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end 
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '12' 
    and b.PAR_VIGENCIA = 'S'   

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END
	 

/* ********** Variable: Permanencia Cuentas por Cobrar (dias) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,14
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_CTA_CBR,0) = 0 AND ISNULL(a.BIF_CTA_CBR,0) = 0 THEN 'N'	
			    WHEN (a.BIF_CTA_CBR >= (b.BIF_CTA_CBR * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '14'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #ctas_x_cobrar, Variable: Permanencia Cuentas por Cobrar (dias)'
			return -1
		END


		
/* **********************************/

Print 'Crea tabla temporal #ctas_x_cobrar'
CREATE TABLE #ctas_x_cobrar
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null

)		

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #ctas_x_cobrar'
				RETURN -1
	END

	
/* ********** Variable: Permanencia Cuentas por Cobrar (dias) ********** */
INSERT INTO #ctas_x_cobrar
SELECT 
	 a.RUT
	,15
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_CTA_CBR,0) = 0 AND ISNULL(a.BIF_CTA_CBR,0) = 0 THEN 'N'	
			    WHEN (a.BIF_CTA_CBR >= (b.BIF_CTA_CBR * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '15'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #ctas_x_cobrar, Variable: Permanencia Cuentas por Cobrar (dias)'
			return -1
		END

	CREATE UNIQUE CLUSTERED INDEX x_ctas_x_cobrar1
    ON #ctas_x_cobrar(RUT,COD_INDICADOR)

 /* ********** Variable: Cuentas Por Cobrar: CC representa mes del 20% del Total de Activos ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,111
	,case when a.BIF_CTA_CBR > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end 
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '111' 
    and b.PAR_VIGENCIA = 'S'   

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END
	 
		
		
/* Deja la alerta mas grave, en caso que el alerta mas debil este contenida en la mas grave */
INSERT INTO #tabla_indicador
SELECT 
    RUT 
    ,MAX(COD_INDICADOR)
    ,ESTADO
FROM 
    #ctas_x_cobrar
WHERE 
    ESTADO = 'S'
GROUP BY RUT,ESTADO 
		
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Permanencia Cuentas por Cobrar (dias)'
			return -1
		END		


/* ********** Variable: Permanencia Inventario (dias) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,18
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_PER_INV,0) = 0 AND ISNULL(a.BIF_PER_INV,0) = 0 THEN 'N'
			    WHEN (a.BIF_PER_INV >= (b.BIF_PER_INV * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '18'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #perm_inven, Variable: Permanencia Inventario (dias) '
			return -1
		END		

		

 /* ********** Variable: Permanencia Inventario (dias)	********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,19
    ,case when a.BIF_VTA_ANU <> 0 then  
            case when a.BIF_PER_INV >= (CONVERT(INT,b.PAR_ADICIONAL)) then 'S' else 'N' end 
          else 'N' 
		  end
    
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '19' 
    and b.PAR_VIGENCIA = 'S'       

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END
		


Print 'Crea tabla temporal #perm_ctas_x_pagar'
CREATE TABLE #perm_ctas_x_pagar
(
	 RUT				int 	 		not null		
	,COD_INDICADOR  	smallint    	not null
	,ESTADO				char (1)		null
	,primary key (RUT,COD_INDICADOR)
)		

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #perm_ctas_x_pagar'
				RETURN -1
	END

	
	
/* Deja la alerta mas grave, en caso que el alerta mas debil este contenida en la mas grave */
INSERT INTO #tabla_indicador
SELECT 
    RUT
    ,MAX(COD_INDICADOR)
    ,ESTADO
FROM 
    #perm_ctas_x_pagar
WHERE
    ESTADO = 'S'
GROUP BY RUT,ESTADO


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Permanencia de creditos'
			return -1
		END	


/* ********** Variable: Leverage ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,23
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_IND_LVG,0) = 0 AND ISNULL(a.BIF_IND_LVG,0) = 0 THEN 'N'
			WHEN (a.BIF_IND_LVG - b.BIF_IND_LVG) >= (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '23.1') 
                and (a.BIF_IND_LVG - b.BIF_IND_LVG) <= (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '23.2') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage '
			return -1
		END		


/* ********** Variable: Leverage periodo anterior >= 1,5 --NUEVA apoyo ********** */
 INSERT INTO #tabla_indicador
SELECT 
	 b.RUT
	,100
	,CASE	WHEN ISNULL(b.BIF_IND_LVG,0) = 0 THEN 'N'
			WHEN (b.BIF_IND_LVG) >= (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '100')  
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_ant b
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage '
			return -1
		END		
		

/* ********** Variable: Leverage ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,24
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_IND_LVG,0) = 0 AND ISNULL(a.BIF_IND_LVG,0) = 0 THEN 'N'
			    WHEN (a.BIF_IND_LVG - b.BIF_IND_LVG) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '24') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	

	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage '
			return -1
		END		
	


/* ********** Variable: Ventas anuales (Ingreso de explotacion) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,30
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_VTA_ANU,0) = 0 AND ISNULL(a.BIF_VTA_ANU,0) = 0 THEN 'N'
			    WHEN (a.BIF_VTA_ANU > (b.BIF_VTA_ANU * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '30'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ventas anuales (Ingreso de explotacion)'
			return -1
		END		

		
		
/* ********** Variable: Ventas anuales (Ingreso de explotacion) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,31
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_VTA_ANU,0) = 0 AND ISNULL(a.BIF_VTA_ANU,0) = 0 THEN 'N'
			    WHEN (a.BIF_VTA_ANU < (b.BIF_VTA_ANU * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '31'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ventas anuales (Ingreso de explotacion)'
			return -1
		END		


/* ********** Variable: Ventas Anuales ( Ingreso de explotacion) --NUEVA apoyo ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,102
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_VTA_ANU,0) = 0 AND ISNULL(a.BIF_VTA_ANU,0) = 0 THEN 'N'
			    WHEN (a.BIF_VTA_ANU - b.BIF_VTA_ANU) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '102.1')
				AND a.BIF_VTA_ANU < (b.BIF_VTA_ANU * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '102.2'))  
					THEN 'S' ELSE 'N' END 

FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Ventas anuales (Ingreso de explotacion)'
			return -1
		END				


/* ********** Variable: Cuentas por cobrar empresas relacionadas ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,33
	,case when a.BIF_CTA_CBR_ER > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end
FROM
	 #balance_act a 
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '33' 
    and b.PAR_VIGENCIA = 'S'       

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END
		

		
/* ********** Variable: Cuentas por cobrar empresas relacionadas ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,34
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_CTA_CBR_ER,0) = 0 AND ISNULL(a.BIF_CTA_CBR_ER,0) = 0 THEN 'N'
			    WHEN (a.BIF_CTA_CBR_ER > (b.BIF_CTA_CBR_ER * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '34'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Cuentas por cobrar empresas relacionadas'
			return -1
		END		


/* ********** Variable: Inversion en empresas relacionadas ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,35
	,case when a.BIF_INV_ER > (a.BIF_TOT_ACT * (CONVERT(DECIMAL(4,2),b.PAR_ADICIONAL))) then 'S' else 'N' end
FROM
	 #balance_act a
    ,APR_PARAMETRO b
WHERE 
        b.PAR_CONCEPTO = 'INDICADORES' 
    and b.PAR_CODIGO = '35' 
    and b.PAR_VIGENCIA = 'S'       

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #balance_act'
			return -1
		END	 
		
		
/* ********** Variable: Inversion en empresas relacionadas ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,36
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_INV_ER,0) = 0 AND ISNULL(a.BIF_INV_ER,0) = 0 THEN 'N'
			    WHEN (a.BIF_INV_ER > (b.BIF_INV_ER * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '36'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Inversion en empresas relacionadas'
			return -1
		END		
		
		
/* ********** Variable: Pasivos exigibles  ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,37
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_PAS_EXG,0) = 0 AND ISNULL(a.BIF_PAS_EXG,0) = 0 THEN 'N'
			    WHEN (a.BIF_PAS_EXG >= (b.BIF_PAS_EXG * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '37'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Pasivos exigibles'
			return -1
		END				
		

/* ********** Variable: Leverage: Incremento >0,5 c/respecto al periodo anterior ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,109
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_IND_LVG,0) = 0 AND ISNULL(a.BIF_IND_LVG,0) = 0 THEN 'N'
			WHEN (a.BIF_IND_LVG - b.BIF_IND_LVG) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '109') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage '
			return -1
		END		
		
/* **************************************** */
/*      		APR_HIST_DEUDASBIF 					*/
/* **************************************** */

Print 'Crea tabla temporal #deudasbif_act'
CREATE TABLE #deudasbif_act
(
	RUT							INT 	 		
	,PERIODO_PROC				DECIMAL(16,0)
	,SBF_MTO_DEU_DIR_SBIF		DECIMAL(18,4) 

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudasbif_act'
				RETURN -1
	END


	
Print 'Insert a tabla #deudasbif_act datos de APR_HIST_DEUDASBIF'
INSERT INTO #deudasbif_act
SELECT 
     RUT
	,PERIODO_PROC
	,SBF_MTO_DEU_DIR_SBIF	
FROM APR_HIST_DEUDASBIF 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODOACT' and PAR_VIGENCIA = 'S')


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #deudasbif_act'
			return -1
		END		
		
	CREATE UNIQUE CLUSTERED INDEX x_deudasbif_act1
    ON #deudasbif_act(RUT,PERIODO_PROC)
	
	CREATE INDEX x_deudasbif_act1_2
    ON #deudasbif_act(RUT)

Print 'Crea tabla temporal #deudasbif_ult'
CREATE TABLE #deudasbif_ult
(
	 RUT						INT 	 		
	,PERIODO_PROC				DECIMAL(16,0)
	,SBF_MTO_DEU_DIR_SBIF		DECIMAL(18,4)  --tb se calcula con la tabla Balance

)

	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #deudasbif_ult'
				RETURN -1
	END



Print 'Insert a tabla #deudasbif_ult datos de APR_HIST_DEUDASBIF los ultimos 6 meses'
INSERT INTO #deudasbif_ult
SELECT 
     RUT
	,PERIODO_PROC
	,SBF_MTO_DEU_DIR_SBIF	
FROM APR_HIST_DEUDASBIF 	
WHERE 
	PERIODO_PROC = (SELECT convert(int,PAR_ADICIONAL) FROM APR_PARAMETRO where PAR_CODIGO = 'PERIODO6ANT' and PAR_VIGENCIA = 'S')	

	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #deudasbif_ult'
			return -1
		END	
		
	CREATE UNIQUE CLUSTERED INDEX x_deudasbif_ult1
    ON #deudasbif_ult(RUT,PERIODO_PROC)

	CREATE INDEX x_deudasbif_ult1_2
    ON #deudasbif_ult(RUT)
	
/* ********** Variable: Deuda SBIF Directa ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,40
    ,CASE WHEN a.SBF_MTO_DEU_DIR_SBIF > ((b.BIF_VTA_ANU/12 )* 6) THEN 'S' ELSE 'N' END
from 
     #deudasbif_act a
	,#balance_act b
where
	a.RUT = b.RUT

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #deudasbif_act'
			return -1
		END	
	

/* ********** Variable: Deuda SBIF Directa  ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,41
	,CASE 	WHEN ISNULL(b.SBF_MTO_DEU_DIR_SBIF,0) = 0 AND ISNULL(a.SBF_MTO_DEU_DIR_SBIF,0) = 0 THEN 'N'
			    WHEN (a.SBF_MTO_DEU_DIR_SBIF > (b.SBF_MTO_DEU_DIR_SBIF * (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '41'))) 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #deudasbif_act a
LEFT JOIN
	 #deudasbif_ult b
ON 
	a.RUT = b.RUT	
	

		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Deuda SBIF Directa'
			return -1
		END	
		


/* ********** Variable: Leverage Financiero (Leverage financieron es > 2) ********** */

INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,115
	,CASE 
		 WHEN (a.BIF_IND_LVG_FIN) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '115') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage Financiero '
			return -1
		END	
		

/* ********** Variable: Leverage Financiero (Incremento en leverage financiero> 0,5 respecto a periodo anterior) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,116
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_IND_LVG_FIN,0) = 0 AND ISNULL(a.BIF_IND_LVG_FIN,0) = 0 THEN 'N'
			    WHEN (a.BIF_IND_LVG_FIN - b.BIF_IND_LVG_FIN) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '116') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage Financiero '
			return -1
		END	
		

/* ********** Variable: Leverage Financiero (Incremento en leverage financiero> 1 respecto a periodo anterior) ********** */
INSERT INTO #tabla_indicador
SELECT 
	 a.RUT
	,117
	,CASE 	WHEN a.TIPO_BAL_DESC <> b.TIPO_BAL_DESC THEN 'N'
			WHEN ISNULL(b.BIF_IND_LVG_FIN,0) = 0 AND ISNULL(a.BIF_IND_LVG_FIN,0) = 0 THEN 'N'
			    WHEN (a.BIF_IND_LVG_FIN - b.BIF_IND_LVG_FIN) > (SELECT (CONVERT(DECIMAL(4,2),PAR_ADICIONAL)) FROM APR_PARAMETRO where PAR_CONCEPTO = 'INDICADORES' and PAR_CODIGO = '117') 
                    THEN 'S' ELSE 'N' END 
FROM 
	 #balance_act a
LEFT JOIN
	 #balance_ant b
ON 
	a.RUT = b.RUT	
	
	
		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  Insert a tabla #tabla_indicador, Variable: Leverage Financiero '
			return -1
		END	
		
		
	
	
	
/* ********** Variable: Patrimonio: NUEVA (combinacion apoyo  indicadores 5, 24, 93) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,94
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL AND C.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 5
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 24
AND B.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador C
ON U.RUT = C.RUT
AND C.COD_INDICADOR = 93
AND C.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Patrimonio: NUEVA'
			return -1
		END	 
		

/* ********** Variable: Cuentas por cobrar (M$): NUEVA (combinacion apoyo  indicadores 12 y 14) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,95
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 12
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 14
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Cuentas por cobrar (M$): NUEVA'
			return -1
		END	 


/* ********** Variable: Ebitda: NUEVA (combinacion apoyo  indicadores 11 y 29 ) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,96
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 11
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 29
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Ebitda: NUEVA'
			return -1
		END	 


		
/* ********** Variable: Permanencia Inventario (dias): NUEVA (combinacion apoyo  indicadores 18 y 19) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,97
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 18
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 19
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Permanencia Inventario (dias): NUEVA'
			return -1
		END	 
		
		
/* ********** Variable: Leverage: NUEVA (combinacion apoyo  indicadores 23 y 100) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,101
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 23
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 100
AND B.ESTADO = 'S'

			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Leverage: NUEVA'
			return -1
		END	 


/* ********** Variable: Leverage: NUEVA (combinacion apoyo  indicadores 24 y 102) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,103
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 24
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 102
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Leverage: NUEVA'
			return -1
		END	 


/* ********** Variable: Ventas Anuales ( Ingreso de explotacion): NUEVA (combinacion apoyo  indicadores 11 y 31) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,105
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 11
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 31
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Leverage: NUEVA'
			return -1
		END	 


/* ********** Variable: Cuentas por Cobrar empresas relacionadas : NUEVA (combinacion apoyo  indicadores 33 y 34) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,106
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 33
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 34
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Cuentas por Cobrar empresas relacionadas: NUEVA'
			return -1
		END	 


/* ********** Variable: Inversion en empresas relacionadas : NUEVA (combinacion apoyo  indicadores 33 y 34) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,107
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 35
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 36
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Inversion en empresas relacionadas: NUEVA'
			return -1
		END	 

		
/* ********** Variable: Deuda SBIF Directa : NUEVA (combinacion apoyo  indicadores 40 y 41) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,108
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 40
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 41
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Deuda SBIF Directa: NUEVA'
			return -1
		END	 

/* ********** Variable: Pasivos Exigibles : NUEVA (combinacion apoyo  indicadores 37 y 109) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,110
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 109
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 37
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Pasivos Exigibles: NUEVA'
			return -1
		END	 


/* ********** Variable: Permanencia Cuentas por Cobrar (dias) : NUEVA (combinacion apoyo  indicadores 15 y 111) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,114
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 15
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 111
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Permanencia Cuentas por Cobrar (dias): NUEVA'
			return -1
		END	 


/* ********** Variable: Leverage Financiero: NUEVA (combinacion apoyo  indicadores 115 y 116) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,118
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 115
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 116
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Leverage Financiero NUEVA'
			return -1
		END	 

	
/* ********** Variable: Leverage Financiero: NUEVA (combinacion apoyo  indicadores 115 y 117) ********** */
INSERT INTO #tabla_indicador
select 
    U.RUT
    ,119
    ,CASE WHEN A.RUT IS NOT NULL AND B.RUT IS NOT NULL THEN 'S'
        ELSE 'N' END
from
    #balance_act U
LEFT JOIN
    #tabla_indicador A
ON U.RUT = A.RUT
AND A.COD_INDICADOR = 115
AND A.ESTADO = 'S'
LEFT JOIN
    #tabla_indicador B
ON U.RUT = B.RUT
AND B.COD_INDICADOR = 117
AND B.ESTADO = 'S'

	
			IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicado, Variable: Leverage Financiero NUEVA'
			return -1
		END	 
		
	
/* ************************************ */		
/*		tabla temp de salida			*/
/* ************************************ */		
		
Print 'Crea tabla temporal #tab_salida'
CREATE TABLE #tab_salida
(
	 RUT				int 	 		not null		
	,CODIGO				varchar(10)		not null
	,PERIODO_PROC		decimal(16,0)	null
	,PAR_CODIGO			char(20)		null
	,COD_INDICADOR		smallint		null
	,FEC_EJEC			datetime		null

)


	IF @@error != 0 BEGIN
				PRINT 'Msg Error : No ejecuto --> Crea tabla temporal #tab_salida'
				RETURN -1
	END

	
print 'inserta registros a la tabla #tab_salida'	
INSERT INTO #tab_salida
select 
    a.RUT
    ,a.PAR_CODIGO + convert(varchar,b.COD_INDICADOR)
    ,a.PERIODO_PROC
    ,a.PAR_CODIGO 
    ,b.COD_INDICADOR
	,a.FEC_EJEC
from 
    #banca_cliente a
    ,#tabla_indicador b
WHERE 
    a.RUT = b.RUT
and b.ESTADO = 'S'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla #tabla_indicador desde #tab_salida'
			return -1
		END	 

	CREATE UNIQUE CLUSTERED INDEX x_tab_salida1
    ON #tab_salida(RUT,CODIGO)
    
    CREATE INDEX x_tab_salida1_2
    ON #tab_salida(CODIGO)
	
print 'inserta registros a la tabla APR_HIST_INDICADOR'	
INSERT INTO APR_HIST_INDICADOR		
SELECT 
     a.PERIODO_PROC
    ,a.RUT
    ,a.COD_INDICADOR
    ,b.PAR_ADICIONAL
    ,a.FEC_EJEC
	,1
FROM
    #tab_salida a
left join
    APR_PARAMETRO b
on
    a.CODIGO = b.PAR_CODIGO
WHERE 
    b.PAR_CONCEPTO = 'CLASALERTAS'    
AND b.PAR_VIGENCIA = 'S'		
AND b.PAR_ADICIONAL <> 'N/A'


		IF @@error != 0 BEGIN
			PRINT 'Msg Error : No ejecuto -->  inserccion a tabla APR_HIST_INDICADOR'
			return -1
		END

		
END

Drop table #banca_cliente
Drop table #complemento_act
Drop table #complemento_ant
Drop table #tabla_indicador
Drop table #deudabci_act
Drop table #deudabci_ant
Drop table #garantia_act
Drop table #garantia_ant
Drop table #deudabci_ult6
Drop table #balance_act
Drop table #balance_ant
Drop table #deudasbif_act
Drop table #deudasbif_ult
Drop table #tab_salida
Drop table #perm_ctas_x_pagar
Drop table #ctas_x_cobrar
Drop table #nro_cli_ult6
go
sp_procxmode sp_alerta_calc_hist, 'anymode'
go
EXEC sp_procxmode 'sp_alerta_calc_hist','unchained'
go
IF OBJECT_ID('sp_alerta_calc_hist') IS NOT NULL
    PRINT '<<< CREATED PROCEDURE sp_alerta_calc_hist >>>'
ELSE
    PRINT '<<< FAILED CREATING PROCEDURE sp_alerta_calc_hist >>>'
go
GRANT EXECUTE ON dbo.sp_alerta_calc_hist TO mantencion
go
GRANT EXECUTE ON dbo.sp_alerta_calc_hist TO ejecucion
go
