use alertasprev
go

IF OBJECT_ID('dbo.vw_APR_Arch_Riesgo_Det_Pro') IS NOT NULL
BEGIN
    DROP VIEW dbo.vw_APR_Arch_Riesgo_Det_Pro
    IF OBJECT_ID('dbo.vw_APR_Arch_Riesgo_Det_Pro') IS NOT NULL
        PRINT '<<< FAILED DROPPING VIEW dbo.vw_APR_Arch_Riesgo_Det_Pro >>>'
    ELSE
        PRINT '<<< DROPPED VIEW dbo.vw_APR_Arch_Riesgo_Det_Pro >>>'
END
go