IF OBJECT_ID('dbo.vw_APR_Arch_Riesgo') IS NOT NULL
BEGIN
    DROP VIEW dbo.vw_APR_Arch_Riesgo
    IF OBJECT_ID('dbo.vw_APR_Arch_Riesgo') IS NOT NULL
        PRINT '<<< FAILED DROPPING VIEW dbo.vw_APR_Arch_Riesgo >>>'
    ELSE
        PRINT '<<< DROPPED VIEW dbo.vw_APR_Arch_Riesgo >>>'
END
go
CREATE VIEW vw_APR_Arch_Riesgo AS

SELECT 
		 E.PERIODO_PROC     
		,E.RUT

        ,E.CLI_DV
        ,E.CLI_NOMBRE
        ,E.COLOR
        ,E.IMPACTO
        ,E.CLI_COD_EJEC
        ,E.CLI_COD_BANCO
        ,E.CLI_COD_BANCA
        ,E.CLI_DESC_REG
        ,E.CLI_DESC_PLAT
        ,E.CPL_COD_CLA_BCI
		,convert(varchar, E.FECHA_BAL, 112) as FECHA_BAL
		,E.BIF_TIP_BAL
        ,E.CPL_FEC_ULT_APC
		
		,COALESCE(G.SBF_MTO_PRT,0) AS SBF_MTO_PRT
		,COALESCE(G.PSB_CANT_PRT,0) AS PSB_CANT_PRT
		,COALESCE(G.PSB_MTO_MOR_COM,0) AS PSB_MTO_MOR_COM
		,COALESCE(G.PSB_CANT_MOR_COM,0) AS PSB_CANT_MOR_COM
		,COALESCE(G.PSB_MAX_FEC_MOR_COM,'1900-01-01') AS PSB_MAX_FEC_MOR_COM
		,COALESCE(G.PSB_CANT_INF_LAB,0) AS PSB_CANT_INF_LAB
		,COALESCE(G.PSB_MTO_INF_LAB,0) AS PSB_MTO_INF_LAB
		,COALESCE(G.PSB_MAX_MESES_DDA_LAB,0) AS PSB_MAX_MESES_DDA_LAB	
        ,' ' as ColumanExcel_Vacia
		,A.BAL_1
		,A.BAL_2
		,A.BAL_3
		,A.BAL_4
		,A.BAL_5
		,A.BAL_6
		,A.BAL_7
		,A.BAL_9
		,A.BAL_10
		,B.BAL_27
		,B.BAL_28
		,B.BAL_30
		,B.BAL_32
		,B.BAL_38
		,B.BAL_39
		,C.COM_42
		,C.COM_43
		,C.COM_44
		,C.COM_45
		,C.COM_46
		,C.COM_47
		,C.COM_48
		,C.COM_49
		,C.COM_50
		,C.COM_51
		,C.COM_52
		,C.COM_53
		,C.COM_54
		,C.COM_55
		,C.COM_56
		,C.COM_57
		,C.COM_58
		,C.COM_59
		,C.COM_60
		,D.COM_61
		,D.OTR_62
		,D.OTR_63
		,D.COM_64
		,D.COM_65
		,D.COM_66
		,D.OTR_67
		,D.OTR_68
		,D.OTR_69
		,D.OTR_70
		,D.OTR_71
		,D.OTR_72
		,D.OTR_73
		,D.OTR_74
		,D.OTR_75
		,D.OTR_76
		,D.OTR_77
		,D.BAL_78
		,D.OTR_79
		,D.COM_80
		,D.COM_81
		,D.COM_82
		,D.COM_83
		,F.COM_91	--nueva
		,F.COM_92	--nueva
		,F.COM_94	--nueva combinacion
		,F.COM_95	--nueva	combinacion
		,F.COM_96	--nueva	combinacion
		,F.COM_97	--nueva	combinacion
		,F.COM_99	--nueva	combinacion
		,F.COM_101	--nueva	combinacion
		,F.COM_103	--nueva	combinacion
		,F.COM_104	--nueva	combinacion
		,F.COM_105	--nueva	combinacion
		,F.COM_106	--nueva	combinacion
		,F.COM_107	--nueva	combinacion
		,F.COM_108	--nueva	combinacion
		,F.COM_110	--nueva	combinacion
		,F.COM_112	--nueva	
		,F.COM_113	--nueva	
		,F.COM_114	--nueva	combinacion
		,F.COM_118	--nueva	combinacion
		,F.COM_119	--nueva	combinacion
		,CASE WHEN H.RUT IS NULL THEN F.OTR_120 ELSE 'R' END OTR_120
FROM 
        vw_APR_Datos_Cliente E
	INNER JOIN
		APR_TMP_ALERTAS_1_20  A
	ON E.RUT=A.RUT
        AND E.PERIODO_PROC = A.PERIODO_PROC
	INNER JOIN
		APR_TMP_ALERTAS_21_40 B
	ON E.RUT=B.RUT
        AND E.PERIODO_PROC = B.PERIODO_PROC
	INNER JOIN
		APR_TMP_ALERTAS_41_60 C
	ON E.RUT=C.RUT
        AND E.PERIODO_PROC = C.PERIODO_PROC
	INNER JOIN
		APR_TMP_ALERTAS_61_83 D
	ON E.RUT=D.RUT
        AND E.PERIODO_PROC = D.PERIODO_PROC
	INNER JOIN
		APR_TMP_ALERTAS_84_120 F
	ON E.RUT=F.RUT
	AND E.PERIODO_PROC = F.PERIODO_PROC
	INNER JOIN
		APR_HIST_PRTSBIF G
	ON  E.RUT=G.RUT
	AND E.PERIODO_PROC = G.PERIODO_PROC
	LEFT JOIN
		(select distinct RUT, PERIODO_PROC FROM APR_TMP_OPEPRO_COMEX) H
	ON E.RUT=H.RUT
	AND E.PERIODO_PROC = H.PERIODO_PROC 
go
GRANT SELECT ON dbo.vw_APR_Arch_Riesgo TO consultas
go
GRANT DELETE ON dbo.vw_APR_Arch_Riesgo TO mantencion
go
GRANT INSERT ON dbo.vw_APR_Arch_Riesgo TO mantencion
go
GRANT SELECT ON dbo.vw_APR_Arch_Riesgo TO mantencion
go
GRANT UPDATE ON dbo.vw_APR_Arch_Riesgo TO mantencion
go
IF OBJECT_ID('dbo.vw_APR_Arch_Riesgo') IS NOT NULL
    PRINT '<<< CREATED VIEW dbo.vw_APR_Arch_Riesgo >>>'
ELSE
    PRINT '<<< FAILED CREATING VIEW dbo.vw_APR_Arch_Riesgo >>>'
go
