#!/usr/bin/sh
#####################################################################################
# Nombre        	: sh_Apr_Arch_Riesgo.sh                                         #
# Tema         	:                                                                   #
# Descripcion   	: Proceso que Genera archivos de alertas Preventivas de Riesgo  #
# Autor         	: ADA Ltda.                                                     #
# Sintaxis     	: Parameto <Periodo YYYYMM>                                         #
# Fecha         	: 04-10-14                                                      #
# Mantención    	:                                                               #
#####################################################################################
#MANTENCION #1:                                                                     #
# Fecha			:	Agosto 2016                                                     #
# Autor			:	Gabriel Martinez. (Ada Ltda.)                                   #
# Descripcion	:	Se agrega vista con nuevos indicadores de riesgo.               #
#####################################################################################
#MANTENCION #2:  															      	#
#Autor			: Francisco Valenzuela, ADA LTDA - Ing. SW BCI: Alvaro Oportus      #
#Fecha			: 23/12/2016                                         			    #
#Descripcion  	: Mantencion que permite agregar un correlativo al archivo		    #
#				  Balances_IFRS, el cual es informacion de entrada para el 	        #
#				  calculo de riesgo de los diferentes clientes evaluados		    #
#####################################################################################
#MANTENCION #3:  															      	#
#Autor			: Francisco Valenzuela, ADA LTDA - Ing. SW BCI: Alvaro Oportus      #
#Fecha			: 29/12/2021                                         			    #
#Descripcion  	: Mantencion que incorpora un nuevo archivo de salida, el cual 		#
#					contiene el detalle de operaciones prorrogadas, ya sean de		#
#					de moneda nacional y de comex									#
#####################################################################################

#---------------------------------------------------------------------
#  Validacion de parametro
#---------------------------------------------------------------------
if [ "$1" = "" ]
then
	echo ""
	echo "Parametro: <Periodo YYYYMM>"
	echo "Ejemplo: sh sh_Apr_Arch_Riesgo.sh 201201"
	echo ""
	exit 2
fi

#---------------------------------------------------------------------
#  Variables
#---------------------------------------------------------------------

ARCHLOG=`date +'%Y%m%d'.sh_Apr_Arch_Riesgo`
export ARCHLOG

SHELL_LOG=`date +'%Y%m%d'.sh_Apr_Arch_Riesgo.log`
export SHELL_LOG

NOMBRE_SHELL="sh_Apr_Arch_Riesgo.sh"
export NOMBRE_SHELL

server=`uname -n`
Dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`

#---------------------------------------------------------------------------------
# Valida archivos de Ambiente y Funciones
#---------------------------------------------------------------------------------
RutaAmbiente=$HOME/she/Apr_ambiente.amb

if test -s $RutaAmbiente
then
	. $RutaAmbiente
else
	echo ""
	echo "Archivo $RutaAmbiente NO OK"
	exit 2
fi


if test -s $RutaFunc
then
	. $RutaFunc
else
	echo ""
	echo "Archivo $RutaAmbiente NO OK"
	exit 2
fi

#---------------------------------------------------------------------------------
# Valida parametro (periodo)
#---------------------------------------------------------------------------------
valida_Perido $1

#---------------------------------------------------------------------
#  Creacion  y Encabezado  de archivo log
#---------------------------------------------------------------------

#Elimina archivo log en caso que la ejecucion anterior presento errores
Eliminar_Archivo $DirLog/$ARCHLOG
Eliminar_Archivo $DirLog/$SHELL_LOG

printlog "*********************************************************************"
printlog "* Nombre shell: sh_Apr_Arch_Riesgo.sh                                 *"
printlog "* Descripcion : Proceso que archivos de alertas Preventivas de Riesgo    *"
printlog "* Ambiente    : "$server ":" $AMB
printlog "* Inicio      : "$Dia " Hora : " $Hora
printlog "*                                                                   *"
printlog "*********************************************************************"
printlog " "

date '+%Y-%m-%d %H:%M:%S' >> $DirLog/$ARCHLOG
echo "INICIO shell sh_Apr_Arch_Riesgo.sh"

#-------------------------------------------------------------------------------------------
#  Obteniendo usuario y password para ambiente sybase BD BHENativas
#-------------------------------------------------------------------------------------------

Realiza_Conexion $UyPuser 



printlog "*****************************************************"
printlog "*    Valida Parametro Versus tabla Fecha      *"
printlog "*****************************************************"

Ultimo_Periodo $bd $tbl_fecha

echo $Ult_Periodo


if [ $1 != $Ult_Periodo ]
	then
		printlog "...Periodo ingresado no corresponde con los campos Calculados"
		Mensaje "...Periodo ingresado no corresponde con los campos Calculados"
		exit 2
	else 
		printlog "...Periodo ingresado Corresponde con los campos Calculados"
		Mensaje "...Periodo ingresado Corresponde con los campos Calculados"
fi


printlog ""
printlog "******************************************"
printlog "*    EJECUTA BORRADO DE TABLA         *"
printlog "******************************************"

Realiza_Truncate $bd $tbl_ALERTAS_1
Realiza_Truncate $bd $tbl_ALERTAS_2
Realiza_Truncate $bd $tbl_ALERTAS_3
Realiza_Truncate $bd $tbl_ALERTAS_4
Realiza_Truncate $bd $tbl_ALERTAS_5

printlog "********************************************"
printlog "*    CARGA TABLAS CON INDICADOR  *"
printlog "********************************************"

Realiza_BcpOut $bd vw_APR_Alertas_1_20 $DirTmp/APR_Alertas_1_20.txt ';'
nreg=`wc -l $DirTmp/APR_Alertas_1_20.txt | awk '{ print $1 }'`
if [ $nreg != '0' ]
	then
		Realiza_BcpIn $bd $tbl_ALERTAS_1 $DirTmp/APR_Alertas_1_20.txt ';'
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo Alertas_1_20"  
fi


Realiza_BcpOut $bd vw_APR_Alertas_21_40 $DirTmp/APR_Alertas_21_40.txt ';'
nreg=`wc -l $DirTmp/APR_Alertas_21_40.txt | awk '{ print $1 }'`
if [ $nreg != '0' ]
	then
		Realiza_BcpIn $bd $tbl_ALERTAS_2 $DirTmp/APR_Alertas_21_40.txt ';'
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo Alertas_21_40"  
fi


Realiza_BcpOut $bd vw_APR_Alertas_41_60 $DirTmp/APR_Alertas_41_60.txt ';'
nreg=`wc -l $DirTmp/APR_Alertas_41_60.txt | awk '{ print $1 }'`
if [ $nreg != '0' ]
	then
		Realiza_BcpIn $bd $tbl_ALERTAS_3 $DirTmp/APR_Alertas_41_60.txt ';'
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo Alertas_41_60"  
fi


Realiza_BcpOut $bd vw_APR_Alertas_61_83 $DirTmp/APR_Alertas_61_83.txt ';'
nreg=`wc -l $DirTmp/APR_Alertas_61_83.txt | awk '{ print $1 }'`
if [ $nreg != '0' ]
	then
		Realiza_BcpIn $bd $tbl_ALERTAS_4 $DirTmp/APR_Alertas_61_83.txt ';'
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo Alertas_61_83"  
fi

Realiza_BcpOut $bd vw_APR_Alertas_84_120 $DirTmp/APR_Alertas_84_120.txt ';'
nreg=`wc -l $DirTmp/APR_Alertas_84_120.txt | awk '{ print $1 }'`
if [ $nreg != '0' ]
	then
		Realiza_BcpIn $bd $tbl_ALERTAS_5 $DirTmp/APR_Alertas_84_120.txt ';'
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo Alertas_84_120"  
fi



Eliminar_Archivo $DirTmp/APR_Alertas_1_20.txt
Eliminar_Archivo $DirTmp/APR_Alertas_21_40.txt
Eliminar_Archivo $DirTmp/APR_Alertas_41_60.txt
Eliminar_Archivo $DirTmp/APR_Alertas_61_83.txt
Eliminar_Archivo $DirTmp/APR_Alertas_84_120.txt


printlog ""
printlog "********************************************"
printlog "*    GENERA ARCHIVO $arch_Riesgo           *"
printlog "********************************************"

Realiza_BcpOut $bd vw_APR_Arch_Riesgo $DirTmp/Arch_Riesgo$ext_Arch ';'
nreg=`wc -l $DirTmp/Arch_Riesgo$ext_Arch | awk '{ print $1 }'`

if [ $nreg != '0' ]
	then
		#formatemos archivo de salida
		sed -e 's/[.]/,/g' $DirTmp/Arch_Riesgo$ext_Arch > $DirtOut/$arch_Riesgo'_'$1$ext_Arch
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo $arch_Riesgo'_'$1$ext_Arch"  
fi


printlog ""
printlog "********************************************"
printlog "*    GENERA ARCHIVO $arch_RiesgoExp           *"
printlog "********************************************"

Realiza_BcpOut $bd vw_APR_Datos_Expertos $DirTmp/Arch_RiesgoExp$ext_Arch ';'
nreg=`wc -l $DirTmp/Arch_RiesgoExp$ext_Arch | awk '{ print $1 }'`

if [ $nreg != '0' ]
	then
		#formatemos archivo de salida
		sed -e 's/[.]/,/g' $DirTmp/Arch_RiesgoExp$ext_Arch > $DirtOut/$arch_RiesgoExp'_'$1$ext_Arch
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo $arch_RiesgoExp'_'$1$ext_Arch"  
fi




printlog ""
printlog "********************************************"
printlog "*    GENERA ARCHIVO $arch_Riesgo           *"
printlog "********************************************"

Realiza_BcpOut $bd vw_APR_Arch_Riesgo_Det_Pro $DirTmp/arch_Riesgo_det$ext_Arch ';'
nreg=`wc -l $DirTmp/arch_Riesgo_det$ext_Arch | awk '{ print $1 }'`

if [ $nreg != '0' ]
	then
		#formatemos archivo de salida
		sed -e 's/[.]/,/g' $DirTmp/arch_Riesgo_det$ext_Arch > $DirtOut/$arch_Riesgo_det'_'$1$ext_Arch
	else
		Mensaje "ALERTA: No se generaron operaciones para el archivo $arch_Riesgo_det'_'$1$ext_Arch"  
fi

Eliminar_Archivo $DirTmp/Arch_Riesgo$ext_Arch
Eliminar_Archivo $DirTmp/Arch_RiesgoExp$ext_Arch
Eliminar_Archivo $DirTmp/arch_Riesgo_det$ext_Arch



echo "FIN shell $NOMBRE_SHELL"
date '+%Y-%m-%d %H:%M:%S'
date '+%Y-%m-%d %H:%M:%S' >> $DirLog/$SHELL_LOG
printlog "FIN shell $NOMBRE_SHELL"

exit 0

