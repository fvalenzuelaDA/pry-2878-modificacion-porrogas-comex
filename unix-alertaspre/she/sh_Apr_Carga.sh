#!/usr/bin/sh
#########################################################################################
# Nombre        	: sh_Apr_Carga.sh                                                   #
# Tema         		: Alerta Preventivas Riesgo                                         #
# Descripcion   	: Proceso Carga de datos para tablas de Alerta Preventivas de Riego.#
# Autor         	: ADA Ltda.                                                         #
# Sintaxis     		: Parameto <Periodo YYYYMM>                                         #
# Fecha         	: 17-06-14                                                          #
#########################################################################################
#MANTENCION #1:  															      		#
#Autor			: Francisco Valenzuela, ADA LTDA - Ing. SW BCI: Alvaro Oportus      	#
#Fecha			: 29/12/2021                                         			    	#
#Descripcion  	: Mantencion que incorpora los tipos y descreciones de operaciones		#
#					prorrogadas de moneda nacional y las operaciones prorrogadas 		#
#					enviadas por comex													#
#########################################################################################


#---------------------------------------------------------------------
#  Validacion cantidad de parametro
#---------------------------------------------------------------------
if [ $# -eq 0 -o $# -gt 3 ]; then 

echo 'ERROR DE PARAMETRO(S)'; 
echo "PARA CARGAR TODOS LOS ARCHIVOS: "
echo "Un parametro: Periodo (AAAAMM)"
echo " "
echo "PARA CARGAR UN ARCHIVO: "
echo "Dos parametros: Periodo (AAAAMM); NOMBRE ARCHIVO "
echo "Ejemplo: 'sh_Apr_Carga.sh 201408 APR_Clientes' "
echo " "
echo "PARA REPROCESAR TODOS LOS ARCHIVOS: "
echo "Dos parametros: Periodo (AAAAMM); R o r "
echo " "
echo "PARA REPROCESAR UN ARCHIVO: "
echo "Tres parametros: Periodo (AAAAMM); NOMBRE ARCHIVO ;R o r "
echo "Ejemplo: 'sh_Apr_Carga.sh 201408 APR_Complementario R' "
echo " "

exit 1; 
fi

#---------------------------------------------------------------------
#  Variables
#---------------------------------------------------------------------
ARCHLOG=`date +'%Y%m%d'.sh_Apr_Carga`
export ARCHLOG

SHELL_LOG=`date +'%Y%m%d'.sh_Apr_Carga.log`
export SHELL_LOG

NOMBRE_SHELL="sh_Apr_Carga.sh"
export NOMBRE_SHELL

server=`uname -n`
Dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente=$HOME/she/Apr_ambiente.amb

#---------------------------------------------------------------------------------
# Valida archivos de Ambiente y Funciones
#---------------------------------------------------------------------------------
if test -s $RutaAmbiente
then
	. $RutaAmbiente
else
	echo ""
	echo "Archivo $RutaAmbiente NO OK"
	exit 2
fi

if test -s $RutaFunc
then
	. $RutaFunc
else
	echo ""
	echo "Archivo $RutaAmbiente NO OK"
	exit 2
fi


#Elimina archivo log en caso que la ejecucion anterior presento errores
Eliminar_Archivo $DirLog/$ARCHLOG
Eliminar_Archivo $DirLog/$SHELL_LOG


#---------------------------------------------------------------------
#  Creacion  y Encabezado  de archivo log
#---------------------------------------------------------------------

printlog "*********************************************************************"
printlog "* Nombre shell: sh_Apr_Carga.sh                              *"
printlog "* Descripcion : Proceso Carga de datos para tablas de Alerta Preventivas de Riego.  *"
printlog "* Ambiente    : "$server ":" $AMB
printlog "* Inicio      : "$Dia " Hora : " $Hora
printlog "*                                                                   *"
printlog "*********************************************************************"
printlog " "

date '+%Y-%m-%d %H:%M:%S' >> $DirLog/$ARCHLOG
echo "INICIO shell $NOMBRE_SHELL"


#-------------------------------------------------------------------------------------------
#  Obteniendo usuario y password para ambiente sybase BD XXXXXXXXXX
#-------------------------------------------------------------------------------------------

printlog "*****************************************"
printlog "*     OBTIENE DATOS DE CONEXION         *"
printlog "*   A LA BASE DE DATOS alertasprev      *"
printlog "*****************************************"

Realiza_Conexion $UyPuser


printlog "*****************************************"
printlog "*  EJECUCIONES DEL PROCESO SEGUN        *"
printlog "*  LOS PARAMETROS DE ENTRADA            *"
printlog "*****************************************"
Mensaje ""
printlog ""

if    [ $# -eq 1 ]  
	then
		Periodo=$1
		valida_Perido $Periodo
		Mensaje "Se cargaran todos los archivos a las tablas correspondientes para el periodo $1"
		printlog "Se cargaran todos los archivos a las tablas correspondientes para el periodo $1"
		Indicador_Parametro="T"
		export Indicador_Parametro

elif  [ $# -eq 2 ]
	then 
		Periodo=$1
		valida_Perido $Periodo
		Ind_o_Arch=$2
	if [ $Ind_o_Arch = 'R' -o $Ind_o_Arch = 'r' ]
		then
			Mensaje "Se ejecuta reproceso, se volveran a cargar los todos los archivos" 
			printlog "Se ejecuta reproceso, se volveran a cargar los todos los archivos" 
			Indicador_Parametro="RT"
			export Indicador_Parametro
		else
			if [ $Ind_o_Arch != 'APR_Clientes' -a $Ind_o_Arch != 'APR_Complementario' -a $Ind_o_Arch != 'APR_DeudaSbif' -a $Ind_o_Arch != 'APR_ProtestosBci' -a $Ind_o_Arch != 'APR_ProtestosSbif' -a $Ind_o_Arch != 'APR_DeudaBci' -a $Ind_o_Arch != $arch_Balace ]
				then
					Mensaje "Error. Nombre de archivo no corresponde o Indicador de Reproceso Incorrecto"
					printlog "Error. Nombre de archivo no corresponde o Indicador de Reproceso Incorrecto"
					exit 2
			else 
					Mensaje "Se cargara el archivo $2 para el periodo $1"
					printlog "Se cargara el archivo $2 para el periodo $1"
					Indicador_Parametro="A"
					export Indicador_Parametro
			fi
	fi

else 
		Periodo=$1
		valida_Perido $Periodo		
		Ind_o_Arch=$2
		IndRen=$3
		if [ $Ind_o_Arch != 'APR_Clientes' -a $Ind_o_Arch != 'APR_Complementario' -a $Ind_o_Arch != 'APR_DeudaSbif' -a $Ind_o_Arch != 'APR_ProtestosBci' -a $Ind_o_Arch != 'APR_ProtestosSbif' -a $Ind_o_Arch != 'APR_DeudaBci' -a $Ind_o_Arch != $arch_Balace ]
			then
				Mensaje "Error. Nombre de archivo no corresponde"
				printlog "Error. Nombre de archivo no corresponde"
				exit 2
		elif [ $IndRen != 'R' -a $IndRen != 'r' ]
			then 
				Mensaje "Error en el tercer parametro, en caso de Reproceso ingresar R" 
				printlog "Error en el tercer parametro, en caso de Reproceso ingresar R" 
				exit 2
		else 
			Mensaje "Se ejecuta reproceso, se volvera a cargar archivo $2 para el periodo $1"
			printlog "Se ejecuta reproceso, se volvera a cargar archivo $2 para el periodo $1"
			Indicador_Parametro="RA"
			export Indicador_Parametro
		fi
fi


case $Indicador_Parametro in

   T)
		############  Carga Todos los archivos  ############ 
		Mensaje ""
		printlog ""
		Mensaje "Valida la existencia de los archivos de Control"
		printlog "Valida la existencia de los archivos de Control"
		Mensaje ""
		printlog ""
		
		Valida_Archivo $DirtOut/$arch_Clientes'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_Comp'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_DeuSbif'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_ProtBci'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_ProtSbif'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_DeuBci'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_Balace'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_ProRen_Cod'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_OpePro_comex'_'$arch_Con$ext_Con

		############  Valida archivos de entrada vs el archivo de control  ############ 
		Mensaje ""
		printlog ""
		Mensaje "Valida la existencia de archivos de entrada y se valida versus archivo Control"		
		printlog "Valida la existencia de archivos de entrada y se valida versus archivo Control"
		Mensaje ""
		printlog ""
		
		#Valida archivo de Clientes
		ArmaNomArch $arch_Clientes
		Cuenta_reg_bd $bd $tbl_cliente $sPerACC
		ValidaRegTbl $tbl_cliente $sPerACC
		#nombre Archivo
		Arch_Cli=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Cli=$iCntACC

		#Valida archivo de Complemento
		ArmaNomArch $arch_Comp
		Cuenta_reg_bd $bd $tbl_compl $sPerACC
		ValidaRegTbl $tbl_compl $sPerACC
		#nombre Archivo
		Arch_Com=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Comp=$iCntACC
		
		#Valida archivo DEUDASBIF
		ArmaNomArch $arch_DeuSbif
		Cuenta_reg_bd $bd $tbl_deudasbif $sPerACC
		ValidaRegTbl $tbl_deudasbif $sPerACC
		#nombre Archivo
		Arch_Dsbif=$Arch_Entrada		
		#nro registros segun el archivo de control
		Nro_Dsbif=$iCntACC

		#Valida archivo DEUDABCI
		ArmaNomArch $arch_DeuBci
		Cuenta_reg_bd $bd $tbl_deudabci $sPerACC
		ValidaRegTbl $tbl_deudabci $sPerACC
		#nombre Archivo
		Arch_Dbci=$Arch_Entrada	
		Nro_Dbci=$iCntACC

		#Valida archivo PROTESTOSBCI
		ArmaNomArch $arch_ProtBci
		Cuenta_reg_bd $bd $tbl_prtbci $sPerACC
		ValidaRegTbl $tbl_prtbci $sPerACC
		#nombre Archivo
		Arch_Pbci=$Arch_Entrada	
		Nro_Probci=$iCntACC
		
		#Valida archivo PROTESTOSSBIF
		ArmaNomArch $arch_ProtSbif
		Cuenta_reg_bd $bd $tbl_prtsbif $sPerACC
		ValidaRegTbl $tbl_prtsbif $sPerACC
		#nombre Archivo
		Arch_Psbif=$Arch_Entrada	
		Nro_Prosbif=$iCntACC		

		#Valida archivo Balance
		ArmaNomArch $arch_Balace
		Cuenta_reg_bd $bd $tbl_balance $sPerACC
		
		#nombre Archivo
		Arch_Balan=$Arch_Entrada	
		Nro_Balanc=$iCntACC		

		#Valida archivo operaciones prorrogas con tipo y descripciones
		ArmaNomArch $arch_ProRen_Cod
		Cuenta_reg_bd $bd $tbl_opeproren_cod $sPerACC
		ValidaRegTbl $tbl_opeproren_cod $sPerACC
		#nombre Archivo
		Arch_Cproren=$Arch_Entrada	
		Nro_Cproren=$iCntACC		

		#Valida archivo operaciones prorrogas comex
		ArmaNomArch $arch_OpePro_comex
		Cuenta_reg_bd $bd $tbl_opepro_comex $sPerACC
		ValidaRegTbl $tbl_opepro_comex $sPerACC
		#nombre Archivo
		Arch_Opecomex=$Arch_Entrada	
		Nro_Opecomex=$iCntACC		
				
		
		############ Realiza la carga de los archivos de entrada a sus respectivas tablas ############ 
		Mensaje ""
		printlog ""
		Mensaje "Realiza la carga de los archivos de entrada a sus respectivas tablas para periodo $sPerACC"		
		printlog "Realiza la carga de los archivos de entrada a sus respectivas tablas para periodo $sPerACC"
		Mensaje ""
		printlog ""
		
		printlog "******************************************"
		printlog "*    CARGA DE ARCHIVOS EN TABLAS         *"
		printlog "******************************************"

		#Carga archivo Clientes
		Realiza_BcpIn $bd $tbl_cliente $DirtOut/$Arch_Cli ';'		
		Cuenta_reg_bd $bd $tbl_cliente $sPerACC
		#Nro de registros cargados en BD
		Nro_CliBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cli $Nro_CliBD $tbl_cliente 

		#Carga archivo Complemento
		Realiza_BcpIn $bd $tbl_compl  $DirtOut/$Arch_Com ';'
		Cuenta_reg_bd $bd $tbl_compl $sPerACC
		#Nro de registros cargados en BD
		Nro_CompBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Comp $Nro_CompBD $tbl_compl 

		#Carga archivo DeudaSbif
		Realiza_BcpIn $bd $tbl_deudasbif  $DirtOut/$Arch_Dsbif ';'
		Cuenta_reg_bd $bd $tbl_deudasbif $sPerACC
		#Nro de registros cargados en BD
		Nro_DSbifBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Dsbif $Nro_DSbifBD $tbl_deudasbif 

		#Carga archivo DeudaBci
		Realiza_BcpIn $bd $tbl_deudabci  $DirtOut/$Arch_Dbci ';'
		Cuenta_reg_bd $bd $tbl_deudabci $sPerACC
		#Nro de registros cargados en BD
		Nro_DBciBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Dbci $Nro_DBciBD $tbl_deudabci 

		#Carga archivo ProtestosBci
		Realiza_BcpIn $bd $tbl_prtbci  $DirtOut/$Arch_Pbci ';'
		Cuenta_reg_bd $bd $tbl_prtbci $sPerACC
		#Nro de registros cargados en BD
		Nro_PBciBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Probci $Nro_PBciBD $tbl_prtbci 

		#Carga archivo ProtestosSbif
		Realiza_BcpIn $bd $tbl_prtsbif  $DirtOut/$Arch_Psbif ';'
		Cuenta_reg_bd $bd $tbl_prtsbif $sPerACC
		#Nro de registros cargados en BD
		Nro_PSbifBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Prosbif $Nro_PSbifBD $tbl_prtsbif 

		#borra tabla de balance, no contiene historia
		Realiza_Truncate $bd $tbl_balance
		#Carga archivo Balances
		Realiza_BcpIn $bd $tbl_balance  $DirtOut/$Arch_Balan ';'
		Cuenta_reg_bd $bd $tbl_balance $sPerACC
		#Nro de registros cargados en BD
		Nro_BalanBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Balanc $Nro_BalanBD $tbl_balance 

		#borra tabla de operaciones prorrogas con tipos y descripcion, no contiene historia
		Realiza_Truncate $bd $tbl_opeproren_cod
		#Carga archivo Balances
		Realiza_BcpIn $bd $tbl_opeproren_cod  $DirtOut/$Arch_Cproren ';'
		Cuenta_reg_bd $bd $tbl_opeproren_cod $sPerACC
		#Nro de registros cargados en BD
		Nro_opeproren_codBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cproren $Nro_opeproren_codBD $tbl_opeproren_cod 

		#borra tabla de operaciones prorrogas comex, no contiene historia
		Realiza_Truncate $bd $tbl_opepro_comex
		#Carga archivo Balances
		Realiza_BcpIn $bd $tbl_opepro_comex  $DirtOut/$Arch_Opecomex ';'
		Cuenta_reg_bd $bd $tbl_opepro_comex $sPerACC
		#Nro de registros cargados en BD
		Nro_opecomexBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Opecomex $Nro_opecomexBD $tbl_opepro_comex 

		
	;; #fin condicion T

   RT)
		############  Reprocesa la Carga de Todos los archivos  ############ 
		Mensaje "Valida la existencia de los archivos de Control"
		printlog "Valida la existencia de los archivos de Control"

		Valida_Archivo $DirtOut/$arch_Clientes'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_Comp'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_DeuSbif'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_ProtBci'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_ProtSbif'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_DeuBci'_'$arch_Con$ext_Con
		Valida_Archivo $DirtOut/$arch_Balace'_'$arch_Con$ext_Con
		
		############  Valida archivos de entrada vs el archivo de control  ############ 
		Mensaje "Valida la existencia de archivos de entrada y se valida versus archivo Control"		
		printlog "Valida la existencia de archivos de entrada y se valida versus archivo Control"

		#Valida archivo de Clientes
		ArmaNomArch $arch_Clientes
		# nombre Archivo
		Arch_Cli=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Cli=$iCntACC
		#borra tabla para la fecha de ejecucion
		BorraTblFeje $bd $tbl_cliente $sPerACC

		
		#Valida archivo de Complemento
		ArmaNomArch $arch_Comp
		#nombre Archivo
		Arch_Com=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Comp=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_compl $sPerACC
		
		#Valida archivo DEUDASBIF
		ArmaNomArch $arch_DeuSbif
		#nombre Archivo
		Arch_Dsbif=$Arch_Entrada	
		#nro registros segun el archivo de control
		Nro_Dsbif=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_deudasbif $sPerACC

		#Valida archivo DEUDABCI
		ArmaNomArch $arch_DeuBci
		#nombre Archivo
		Arch_Dbci=$Arch_Entrada	
		#nro registros segun el archivo de control		
		Nro_Dbci=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_deudabci $sPerACC
		
		#Valida archivo PROTESTOSBCI
		ArmaNomArch $arch_ProtBci
		#nombre Archivo
		Arch_Pbci=$Arch_Entrada	
		Nro_Probci=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_prtbci $sPerACC

		#Valida archivo PROTESTOSSBIF
		ArmaNomArch $arch_ProtSbif
		#nombre Archivo
		Arch_Psbif=$Arch_Entrada
		Nro_Prosbif=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_prtsbif $sPerACC

		#Valida archivo BALANCES
		ArmaNomArch $arch_Balace
		#nombre Archivo
		Arch_Balan=$Arch_Entrada
		Nro_Balanc=$iCntACC
		#borra tabla para periodo a cargar
		BorraTblFeje $bd $tbl_balance $sPerACC
		
		#Valida archivo operaciones prorrogas con tipo y descripciones
		ArmaNomArch $arch_ProRen_Cod
		Cuenta_reg_bd $bd $tbl_opeproren_cod $sPerACC
		ValidaRegTbl $tbl_opeproren_cod $sPerACC
		#nombre Archivo
		Arch_Cproren=$Arch_Entrada	
		Nro_Cproren=$iCntACC		

		#Valida archivo operaciones prorrogas comex
		ArmaNomArch $arch_OpePro_comex
		Cuenta_reg_bd $bd $tbl_opepro_comex $sPerACC
		ValidaRegTbl $tbl_opepro_comex $sPerACC
		#nombre Archivo
		Arch_Opecomex=$Arch_Entrada	
		Nro_Opecomex=$iCntACC		
		
		############ Realiza la carga de los archivos de entrada a sus respectivas tablas ############ 
		Mensaje "Realiza la carga de los archivos de entrada a sus respectivas tablas para Periodo $sPerACC"		
		printlog "Realiza la carga de los archivos de entrada a sus respectivas tablas para Periodo $sPerACC"

		printlog "******************************************"
		printlog "*    CARGA DE ARCHIVOS EN TABLAS         *"
		printlog "******************************************"

		#Carga archivo Clientes
		Realiza_BcpIn $bd $tbl_cliente $DirtOut/$Arch_Cli ';'
		Cuenta_reg_bd $bd $tbl_cliente $sPerACC
		#Nro de registros cargados en BD
		Nro_CliBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cli $Nro_CliBD $tbl_cliente 
		
		#Carga archivo Complemento
		Realiza_BcpIn $bd $tbl_compl  $DirtOut/$Arch_Com ';'
		Cuenta_reg_bd $bd $tbl_compl $sPerACC
		#Nro de registros cargados en BD
		Nro_CompBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Comp $Nro_CompBD $tbl_compl 
		
		#Carga archivo DeudaSbif
		Realiza_BcpIn $bd $tbl_deudasbif  $DirtOut/$Arch_Dsbif ';'
		Cuenta_reg_bd $bd $tbl_deudasbif $sPerACC
		#Nro de registros cargados en BD
		Nro_DSbifBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Dsbif $Nro_DSbifBD $tbl_deudasbif

		#Carga archivo DeudaBci
		Realiza_BcpIn $bd $tbl_deudabci  $DirtOut/$Arch_Dbci ';'
		Cuenta_reg_bd $bd $tbl_deudabci $sPerACC
		#Nro de registros cargados en BD
		Nro_DBciBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Dbci $Nro_DBciBD $tbl_deudabci 
		
		#Carga archivo ProtestosBci
		Realiza_BcpIn $bd $tbl_prtbci  $DirtOut/$Arch_Pbci ';'
		Cuenta_reg_bd $bd $tbl_prtbci $sPerACC
		#Nro de registros cargados en BD
		Nro_PBciBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Probci $Nro_PBciBD $tbl_prtbci 		
		
		#Carga archivo ProtestosSbif
		Realiza_BcpIn $bd $tbl_prtsbif  $DirtOut/$Arch_Psbif ';'
		Cuenta_reg_bd $bd $tbl_prtsbif $sPerACC
		#Nro de registros cargados en BD
		Nro_PSbifBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Prosbif $Nro_PSbifBD $tbl_prtsbif 

		#borra tabla de balance, no contiene historia
		Realiza_Truncate $bd $tbl_balance
		#Carga archivo balances
		Realiza_BcpIn $bd $tbl_balance  $DirtOut/$Arch_Balan ';'
		Cuenta_reg_bd $bd $tbl_balance $sPerACC
		#Nro de registros cargados en BD
		Nro_BalanBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Balanc $Nro_BalanBD $tbl_balance 

		#borra tabla de operaciones prorrogas con tipos y descripcion, no contiene historia
		Realiza_Truncate $bd $tbl_opeproren_cod
		#Carga archivo Balances
		Realiza_BcpIn $bd $tbl_opeproren_cod  $DirtOut/$Arch_Cproren ';'
		Cuenta_reg_bd $bd $tbl_opeproren_cod $sPerACC
		#Nro de registros cargados en BD
		Nro_opeproren_codBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cproren $Nro_opeproren_codBD $tbl_opeproren_cod 

		#borra tabla de operaciones prorrogas comex, no contiene historia
		Realiza_Truncate $bd $tbl_opepro_comex
		#Carga archivo Balances
		Realiza_BcpIn $bd $tbl_opepro_comex  $DirtOut/$Arch_Opecomex ';'
		Cuenta_reg_bd $bd $tbl_opepro_comex $sPerACC
		#Nro de registros cargados en BD
		Nro_opecomexBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Opecomex $Nro_opecomexBD $tbl_opepro_comex 
		
		
	;; #fin condicion RT		

	
   A)
		############  Carga archivos entrada ############ 
		Mensaje "Valida la existencia del archivo de Control"
		printlog "Valida la existencia del archivo de Control"

		Valida_Archivo $DirtOut/$2'_'$arch_Con$ext_Con

		############  Valida archivo de entrada vs el archivo de control  ############ 
		Mensaje "Valida la existencia de archivo de entrada y se valida versus archivo Control"		
		printlog "Valida la existencia de archivo de entrada y se valida versus archivo Control"

		#Valida archivo de Clientes
		ArmaNomArch $2
		Nombre_tbl $2
		Cuenta_reg_bd $bd $Nombre_Tabla $sPerACC
		ValidaRegTbl $Nombre_Tabla $sPerACC
		#nombre Archivo
		Arch_Cargar=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Cli=$iCntACC
		
		############ Realiza la carga de los archivos de entrada a sus respectivas tablas ############ 
		Mensaje "Realiza la carga de los archivos de entrada a sus respectivas tablas para periodo $sPerACC"		
		printlog "Realiza la carga de los archivos de entrada a sus respectivas tablas para periodo $sPerACC"

		printlog "******************************************"
		printlog "*    CARGA DE ARCHIVOS EN TABLAS         *"
		printlog "******************************************"

		if [ $Arch_Cargar = $arch_Balace ]
			then
				#borra tabla de balance, no contiene historia
				Realiza_Truncate $bd $tbl_balance
		fi

		#Carga archivo Clientes
		Realiza_BcpIn $bd $Nombre_Tabla $DirtOut/$Arch_Cargar ';'		
		Cuenta_reg_bd $bd $Nombre_Tabla $sPerACC
		#Nro de registros cargados en BD
		Nro_RegBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cli $Nro_RegBD $Nombre_Tabla 		
		
	;; #fin condicion A	

   RA)
		############  Reproceso Carga archivos entrada ############ 
		Mensaje "Valida la existencia del archivo de Control"
		printlog "Valida la existencia del archivo de Control"

		Valida_Archivo $DirtOut/$2'_'$arch_Con$ext_Con

		############  Valida archivo de entrada vs el archivo de control  ############ 
		Mensaje "Valida la existencia de archivo de entrada y se valida versus archivo Control"		
		printlog "Valida la existencia de archivo de entrada y se valida versus archivo Control"

		#Valida archivo de Clientes
		ArmaNomArch $2
		Nombre_tbl $2
		#nombre Archivo
		Arch_Cargar=$Arch_Entrada
		#nro registros segun el archivo de control
		Nro_Cli=$iCntACC
		#borra tabla para la fecha de ejecucion
		BorraTblFeje $bd $Nombre_Tabla $sPerACC
		
		
		############ Realiza la carga del archivo de entrada a su respectiva tabla ############ 
		Mensaje "Realiza la carga del archivo de entrada a su respectiva tabla"		
		printlog "Realiza la carga del archivo de entrada a su respectiva tabla"

		printlog "******************************************"
		printlog "*    CARGA ARCHIVO EN TABLA         *"
		printlog "******************************************"

		if [ $Arch_Cargar = $arch_Balace ]
			then
				#borra tabla de balance, no contiene historia
				Realiza_Truncate $bd $tbl_balance
		fi

		#Carga archivo Clientes
		Realiza_BcpIn $bd $Nombre_Tabla $DirtOut/$Arch_Cargar ';'		
		Cuenta_reg_bd $bd $Nombre_Tabla $sPerACC
		#Nro de registros cargados en BD
		Nro_RegBD=$Num_Registros
		#Valida que todos los registros fueron cargados en la tabla
		ValidaCargaRegTbl $Nro_Cli $Nro_RegBD $Nombre_Tabla 		
		
	;; #fin condicion RA	


esac

printlog "Termina la ejecucion de manera Correcta"
Mensaje "Termina la ejecucion de manera Correcta"

printlog ""
Mensaje ""
Mensaje "Fin shell $NOMBRE_SHELL"
date '+%Y-%m-%d %H:%M:%S' >> $DirLog/$ARCHLOG
printlog "Fin Shell $NOMBRE_SHELL"
exit 0
