.logon usuario,password;

/* **************************************************************************************************** */
/*																										*/
/*Nombre Bteq				:34_2_APR_Fexp_Cod_Pror_Ren.sql												*/
/*Nombre BD					:EDW_VW																		*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:13/12/2021																	*/
/*Autor						:ADA LTDA																	*/
/*Objetivos					:Se exporta la informacion de codigos y descripciones que identificab a las	*/  
/*								operaciones prorrogadas													*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_TEMP}																	*/
/*Tabla de entrada			:${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S										*/
/*Archivo de salida			:APR_Prorroga_Desc_yyyymm_yyyymmdd.txt										*/
/*Retorno					:Codigos y descripciones de Operaciones prorrogadas y renegociadas			*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */

.LOGTABLE ${BD_TEMP}.APR_CLI_COMPLEM_EXPORT;
.BEGIN EXPORT;
.EXPORT OUTFILE $ARCH_PRO_REN_COD_DESC
MODE RECORD FORMAT TEXT;
SELECT
(
	(CAST((FEC_PROC							(FORMAT 'YYYYMM'))			AS VARCHAR(10)))	|| ';'	||
	(CAST(ACCOUNT_NUM													AS VARCHAR(18)))	|| ';'	||
	(CAST(ACCOUNT_MODIFIER_NUM											AS VARCHAR(18)))	|| ';'	||
	(CAST(ACCOUNT_TYPE_CD												AS VARCHAR(06))) 	|| ';'	||	
	(CAST(TAB_GLS_DESC													AS VARCHAR(50)))	|| ''
	)
	(CHAR (106))(TITLE '')
 FROM 
	${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S

;
.END EXPORT;

DROP TABLE ${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S;

SEL DATE, TIME;

.LOGOFF;
