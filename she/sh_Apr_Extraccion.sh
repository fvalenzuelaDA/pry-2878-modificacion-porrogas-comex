#!/usr/bin/sh 
#####################################################################################
# Nombre        : sh_Apr_Extraccion.sh                                              #
# Autor         : Rodolfo Madrid Bascuñan (ADA)                                     #
# Fecha         : 02/10/2014                                                        #
# Descripcion   : Automatización que permite ejecutar proceso DWH,                  #
#                 tilizando Utilitarios (BTEQ, FLOAD, FEXPORT)                      #
#####################################################################################
#MANTENCION #1:                                                                     #
# Autor			: ADA LTDA                                                          #
# Fecha			: 03/02/2016                                                        #
# Descripcion	: Se modifica validacion de archivo de control.                     #
#				  Se ctiva validacion de archivos de entrada.                       #
#				  Cambio de nombre y formato de archivo de control.                 #
#####################################################################################
#MANTENCION #2:  															      	#
#Autor			: Francisco Valenzuela, ADA LTDA - Ing. SW BCI: Felix salgado       #
#Fecha			: 23/12/2016                                         			    #
#Descripcion  	: Mantencion que permite agregar un correlativo al archivo		    #
#				  Balances_IFRS, el cual es informacion de entrada para el 	        #
#				  calculo de riesgo de los diferentes clientes evaluados		    #
#####################################################################################
#MANTENCION #3:  															      	#
#Autor			: Francisco Valenzuela, ADA LTDA - Ing. SW BCI: Felix salgado       #
#Fecha			: 13/03/2017                                         			    #
#Descripcion  	: Mantencion que permite no cargar el archivo DETALLETOTCAR_bhv		#
#####################################################################################


#-------------------------------------------------------------------------------------
#  Variables
#-------------------------------------------------------------------------------------

NOMBRE_SHELL="sh_Apr_Extraccion.sh"
server=`uname -n`

fec_ejec_proc=`date +'%Y''%m''%d'`
export fec_ejec_proc

Hora=`date +'%H':'%M':'%S'`

ARCHLOG=`date +'%Y%m%d'.sh_Apr_Extraccion`
export ARCHLOG

SHELL_LOG=`date +'%Y%m%d'.sh_Apr_Extraccion.log`
export SHELL_LOG

fecha=`date +'%Y%m%d'`
export fecha



RutaAmbiente=$HOME/she/Apr_ambiente.amb



#Ejecuta archivo ambiente
#Valida la existencia del archivo de Configuracion

if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	exit 2
fi

if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
	
	
echo "esta es el archivo de funciones: " $RutaFunc

else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	exit 2
fi


if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	exit 2
fi

if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos 1: $RutaFunc"
	exit 2
fi


###################################################################
#			  Valida formato del periodo de entrada				  #
#			  Valida Archivo periodo de entrada				  	  #
###################################################################

if [ $# -eq 0 -o $# -gt 2 ]
then  
userMsg_rm
exit 2
fi

#Validacion de Parametro
if [ $# -ge 1 ]
then
	valida_Perido $1
else
	userMsg_rm
	exit 2
fi

if [ $# -eq 2 ]
then
	Ejec_Parcial=`echo $2 | tr '[:lower:]' '[:upper:]'` 
	if [ $Ejec_Parcial != 'BATCH' -a	 $Ejec_Parcial != 'PROT' ]
	then
		userMsg_rm
		exit 2
	fi
fi

periodo=$1
export periodo
periodo2=`echo ${periodo}  | awk  '{anno=substr($1,1,4);mes=substr($1,5,2); mes=mes-1; if(mes<=0){anno=anno-1}; if(mes<=0){mes=mes+12}; if(mes<0){mes=mes+11}; printf("%s%02d\n", anno,mes); };'`
export periodo2
FecPrc=${periodo}01
export FecPrc


#Nombre de Archivos Salida finales, Ejecuccion Completa.
#1- Archivo de clientes, datos basicos
ARCH_CLIENTES=$DirtOut/${arch_Clientes}_${periodo}_${fec_ejec_proc}.csv
export ARCH_CLIENTES
#2- Archivo deuda BCI
ARCH_DUE_BCI=$DirtOut/${arch_DeuBci}_${periodo}_${fec_ejec_proc}.csv
export ARCH_DUE_BCI
#3- Archivo deuda SBif
ARCH_DEU_SBIF=$DirtOut/${arch_DeuSbif}_${periodo}_${fec_ejec_proc}.csv
export ARCH_DEU_SBIF
#4- Archivo Complementario de Clientes
ARCH_COM_CLI=$DirtOut/${arch_Comp}_${periodo}_${fec_ejec_proc}.csv
export ARCH_COM_CLI
#5- Archivo Protestos BCI
ARCH_PROT_BCI=$DirtOut/${arch_ProtBci}_${periodo}_${fec_ejec_proc}.csv
export ARCH_PROT_BCI
#6- Archivo Protestos SBIF
ARCH_PROT_SBIF=$DirtOut/${arch_ProtSbif}_${periodo}_${fec_ejec_proc}.csv
export ARCH_PROT_SBIF
#7- Archivo de Balances
ARCH_BALANCE=$DirtOut/${arch_Balace}_${periodo}_${fec_ejec_proc}.csv
export ARCH_BALANCE
#7- Archivo de Balances
ARCH_PRO_REN_COD_DESC=$DirtOut/${arch_ProRen_Cod}_${periodo}_${fec_ejec_proc}.csv
export ARCH_PRO_REN_COD_DESC


#Nombre de Archivos Salida finales, Ejecuccion Parcial BATCH.
#1- Archivo de clientes I
ARCH_BATCH_28=$DirOut/0016BT028${fec_ejec_proc}01
export ARCH_BATCH_28
#2- Archivo de clientes II
ARCH_BATCH_68=$DirOut/0016BT0068I${fec_ejec_proc}N
export ARCH_BATCH_68
#2- Archivo de clientes III
ARCH_BATCH_21=$DirOut/0016BT0021${fec_ejec_proc}N
export ARCH_BATCH_21


#Nombre de Archivos Salida finales, Ejecuccion Parcial Protestos Semanales BCI.
#1- Archivo protestos BCI semanal
ARCH_PROT_BCI_SEM=$DirtOut/${arch_ProtBci}_semanal_${fec_ejec_proc}.csv
export ARCH_PROT_BCI_SEM


# Elimina archivo log en caso que la ejecucion anterior presento errores
Eliminar_Archivo $DirLog/$ARCHLOG
Eliminar_Archivo $DirLog/$SHELL_LOG


printlog "$fec_ejec_proc. INICIO shell sh_Apr_Extraccion.sh"
printlog ""
Mensaje "INICIO shell sh_Apr_Extraccion.sh"
echo ""

#---------------------------------------------------------------------
#  Crecion  y Encabezado archivo log
#---------------------------------------------------------------------
printlog "*************************************************************************" 
printlog "* Nombre shell: sh_Apr_Extraccion.sh                                *" 
printlog "* Descripcion : Ejecución de Proceso Automatizacion ${NOMBRE_SHELL}     *" 
printlog "* Ambiente    : "$server 
printlog "* Inicio      : "$fec_ejec_proc " Hora : " $Hora
printlog "*                                                                       *"
printlog "*************************************************************************"
printlog " "


printlog ""
printlog '** Validando Archivo de Entradas **'
Mensaje "**   Validando Archivos de Entradas   **"
Mensaje "----------------------------------------"
Valida_Archivo $LogonFile

if [ $# -eq 1 ]
then
	Tipo_Ejecuccion=COMPLE
	Valida_Archivo $DirCfg/$arch_proc_extrac
	Valida_Archivo $DirDat/$arch_banca
	# 1- Valida Archivo Entrada detalletotcar_individuales.CSV: Detalle Provisiones Comerciales Individual
	Valida_Archivo $DirIn/$arch_prov_ind
	
	# 2- Valida Archivo Entrada DETALLETOTCAR_provIndGrup.CSV: Detalle Provisiones Comerciales Grupal 
	Valida_Archivo $DirIn/$arch_prov_gru
	
	# 3- Valida Archivo Entrada c3_res_Cardet_IFRS.txt: Deuda BCI 
	Valida_Archivo $DirIn/$arch_res_cardet
	fec_cardet=`awk 'BEGIN {FS="|"}; {print substr($1,1,6)} ' $DirIn/$arch_res_cardet | head -1`
	Num_Lin_res_cardet=`wc -l $DirIn/$arch_res_cardet | awk '{ printf "%i", $1 }'`

	#  4- Valida Archivo Entrada Apr_Factoring_YYYYMM_yyyymmdd.csv: Deuda Factoring BCI
	ls -r $DirIn/${arch_deu_fat_bci}_${periodo}*.ctr > $DirIn/deu_fac.txt
	if test -s $DirIn/deu_fac.txt
	then
		arch_ctr_Fac=`head -1 $DirIn/deu_fac.txt`
		export arch_ctr_Fac
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo control: Deuda Factoring BCI"
		printlog "ERROR: No se encuentra el archivo control: Deuda Factoring BCI"
		echo " "
	exit 2
	fi
	Eliminar_Archivo $DirIn/deu_fac.txt
	Valida_Archivo $arch_ctr_Fac
	#obtiene fecha AAAAMM de proceso del archivo extract
	fec_ctr_fac=`awk '{print substr($0,1,6)}' $arch_ctr_Fac`
	# obtiene cantidad de registros del archivo control del extract
	cant_reg_ctr_fac=`awk '{print substr($0,7,10)}' $arch_ctr_Fac`
	# obtiene fecha de ejecuccion AAAAMMDD del archivo control del extract
	fec_ejec_ctr_fac=`awk '{print substr($0,17,8)}' $arch_ctr_Fac`
	export fec_ejec_ctr_fac
	Valida_Archivo_existe $DirIn/${arch_deu_fat_bci}_${fec_ctr_fac}_${fec_ejec_ctr_fac}.csv

	#  5- Valida Archivo Entrada 0016BT0068Syyyymmdd1: Deuda ACHEL (leasing sistema)
	ls -r $DirIn/${arch_deu_lea}${periodo}* > $DirIn/deu_achel.txt
	if test -s $DirIn/deu_achel.txt
	then
		awk '{ sub(/.CTR/, "");print }' $DirIn/deu_achel.txt > $DirIn/deu_achel_2.txt
		arch_deu_achel=`head -1 $DirIn/deu_achel_2.txt`
		export arch_deu_achel
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo 0016BT0068Syyyymmdd1: Deuda ACHEL (leasing sistema)"
		printlog "ERROR: No se encuentra el archivo 0016BT0068Syyyymmdd1: Deuda ACHEL (leasing sistema)"
		echo " "
	exit 2
	fi
	Eliminar_Archivo $DirIn/deu_achel.txt
	Eliminar_Archivo $DirIn/deu_achel_2.txt
	Valida_Archivo_existe $arch_deu_achel

	# 6- Valida Archivo Entrada Perspectiva_Aprocred_yyyymmdd.csv: Aprocred
	ls -r $DirIn/${arch_aprocred}_${periodo}*.csv > $DirIn/Perspectiva_Aprocred.txt
	if test -s $DirIn/Perspectiva_Aprocred.txt
	then
		arch_Aprocred=`head -1 $DirIn/Perspectiva_Aprocred.txt`
		export arch_Aprocred
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo Perspectiva_Aprocred_YYYYMMDD.csv"
		printlog "ERROR: No se encuentra el archivo Perspectiva_Aprocred_YYYYMMDD.csv"
		echo " "
		exit 2
	fi
	Eliminar_Archivo $DirIn/Perspectiva_Aprocred.txt
	Valida_Archivo_existe $arch_Aprocred

	# 7- Valida Archivo Entrada EPI_Estrategia_yyyymmdd.csv: Epiphany 
	ls -r $DirIn/${arch_epi_est}_${periodo}*.csv > $DirIn/EPI_Estrategia.txt
	if test -s $DirIn/EPI_Estrategia.txt
	then
		arch_EPI_Estrategia=`head -1 $DirIn/EPI_Estrategia.txt`
		export arch_EPI_Estrategia
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo EPI_Estrategia_YYYYMMDD.csv"
		printlog "ERROR: No se encuentra el archivo EPI_Estrategia_YYYYMMDD.csv"
		echo " "
		exit 2
	fi
	Eliminar_Archivo $DirIn/EPI_Estrategia.txt
	Valida_Archivo_existe $arch_EPI_Estrategia


	#  8- Valida Archivo Entrada Ext_Ventas_YYYYMM_yyyymmdd.csv: Ventas Mensuales
	ls -r $DirIn/${arch_ext_ven}_${periodo2}*.ctr > $DirIn/ven_mens.txt
	if test -s $DirIn/ven_mens.txt
	then
		arch_ctr_Ven=`head -1 $DirIn/ven_mens.txt`
		export arch_ctr_Ven
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo control: Ventas Mensuales "
		printlog "ERROR: No se encuentra el archivo control: Ventas Mensuales"
		echo " "
	exit 2
	fi
	Eliminar_Archivo $DirIn/ven_mens.txt
	Valida_Archivo $arch_ctr_Ven
	# obtiene fecha AAAAMM de proceso del archivo extract
	fec_ctr_ven=`awk '{print substr($0,1,6)}' $arch_ctr_Ven`
	# obtiene cantidad de registros del archivo control del extract
	cant_reg_ctr_ven=`awk '{print substr($0,7,12)}' $arch_ctr_Ven`
	# obtiene fecha de ejecuccion AAAAMMDD del archivo control del extract
	fec_ejec_ctr_ven=`awk '{print substr($0,17,8)}' $arch_ctr_Ven`
	export fec_ejec_ctr_ven
	Valida_Archivo_existe $DirIn/${arch_ext_ven}_${fec_ctr_ven}_${fec_ejec_ctr_ven}.csv

	# 9- Valida Archivo Entrada mc_prt_sfs.csv - Protestos Sbif 
	Valida_Archivo_existe $DirIn/$arch_prt_sfs.txt

	# 10- Archivo de entrada faltante Morosos Comercio 
	ls -r $DirIn/${arch_moroso_com}${periodo}* > $DirIn/Moroso_comercio.txt
	if test -s $DirIn/Moroso_comercio.txt
	then
		awk '{ sub(/.CTR/, "");print }' $DirIn/Moroso_comercio.txt > $DirIn/Moroso_comercio_2.txt
		arch_moroso_comercio=`head -1 $DirIn/Moroso_comercio_2.txt`
		export arch_moroso_comercio
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo Morosos Comercio"
		printlog "ERROR: No se encuentra el archivo Morosos Comercio"
		echo " "
		exit 2
	fi
	Eliminar_Archivo $DirIn/Moroso_comercio.txt
	Eliminar_Archivo $DirIn/Moroso_comercio_2.txt
	Valida_Archivo_existe $arch_moroso_comercio



	# 11- Valida Archivo Entrada 0016BT021yyyymmdd01: Infracciones Laborales
	ls -r $DirIn/${arch_info_lab}${periodo}* > $DirIn/Infrac_laborales.txt
	if test -s $DirIn/Infrac_laborales.txt
	then
		awk '{ sub(/.CTR/, "");print }' $DirIn/Infrac_laborales.txt > $DirIn/Infrac_laborales_2.txt
		arch_infrac_lab=`head -1 $DirIn/Infrac_laborales_2.txt`
		export arch_infrac_lab
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo 0016BT021yyyymmdd01: Infracciones Laborales"
		printlog "ERROR: No se encuentra el archivo 0016BT021yyyymmdd01: Infracciones Laborales"
		echo " "
		exit 2
	fi
	Eliminar_Archivo $DirIn/Infrac_laborales.txt
	Eliminar_Archivo $DirIn/Infrac_laborales_2.txt
	Valida_Archivo_existe $arch_infrac_lab



	# 12- Valida Archivo Entrada BIF_Balances_IFRS_yyyymmdd.csv: Balances IFRS
	# ordena y seleciona el archivo control con la fecha maxima 
	ls -r $DirIn/${arch_bal_ifrs}_${periodo}*.csv > $DirIn/balance_ifrs.txt
	if test -s $DirIn/balance_ifrs.txt
	then
		arch_balan_ifrs=`head -1 $DirIn/balance_ifrs.txt`
		export arch_balan_ifrs
	else
		echo " "
		Mensaje "ERROR: No se encuentra el archivo BIF_Balances_IFRS_yyyymmdd.csv"
		printlog "ERROR: No se encuentra el archivo BIF_Balances_IFRS_yyyymmdd.csv"
		echo " "
		exit 2
	fi
	Eliminar_Archivo $DirIn/balance_ifrs.txt
	Valida_Archivo_existe $arch_balan_ifrs
	awk 'BEGIN {FS=";"}; {print NR";"$0}' $arch_balan_ifrs > $DirTmp/arch_balan_ifrs_tmp.txt
	mv $arch_balan_ifrs $DirTmp/balan_ifrs_resp.txt 
	mv $DirTmp/arch_balan_ifrs_tmp.txt $arch_balan_ifrs

	# Formateo de Archivos, Elimina Encabezado de archivo y reemplaza "," por "."
	sed '1d' $DirIn/$arch_prov_ind | sed -e 's/[,]/./g' > $DirTmp/detalletotcar_individuales_F.csv
	sed '1d' $DirIn/$arch_prov_gru | sed -e 's/[,]/./g' > $DirTmp/DETALLETOTCAR_provIndGrup_F.csv

	#reemplaza "-" por "|" 
	sed -e 's/[-]/|/g' $arch_EPI_Estrategia > $DirTmp/EPI_Estrategia_F.csv
	sed -e 's/[ ]//g' $DirIn/$arch_prt_sfs.txt > $DirTmp/${arch_prt_sfs}_t.txt
	#  Elimina Encabezado 
	sed '1d' $arch_deu_achel > $DirTmp/0016BT0068S_T
	sed '1d' $arch_infrac_lab  > $DirTmp/0016BT021_T
	sed '1d' $arch_moroso_comercio  > $DirTmp/0016BT028_T
	
	
	printlog "** Ejecutando Proceso extraccion Alertas Preventivas Riesgo...**"
	printlog "----------------------------------------------------------------"
	echo ""
	echo ""
	Mensaje "** Ejecutando Proceso extraccion Alertas Preventivas Riesgo...**"
	Mensaje "----------------------------------------------------------------"

fi

if [ $# -eq 2 ]
then
	if [ $Ejec_Parcial = 'BATCH' ]
		then
		Tipo_Ejecuccion=BATCH
		arch_proc_extrac=Archivo_Procesos_Extract_APR_batch.txt
		Valida_Archivo $DirCfg/$arch_proc_extrac
		Valida_Archivo $DirDat/$arch_banca
		# 1- Valida Archivo Entrada detalletotcar_individuales.CSV: Detalle Provisiones Comerciales Individual
		Valida_Archivo $DirIn/$arch_prov_ind
		# 2- Valida Archivo Entrada DETALLETOTCAR_provIndGrup.CSV: Detalle Provisiones Comerciales Grupal 
		Valida_Archivo $DirIn/$arch_prov_gru
		
		# Formateo de Archivos, Elimina Encabezado de archivo y reemplaza "," por "."
		sed '1d' $DirIn/$arch_prov_ind | sed -e 's/[,]/./g' > $DirTmp/detalletotcar_individuales_F.csv
		sed '1d' $DirIn/$arch_prov_gru | sed -e 's/[,]/./g' > $DirTmp/DETALLETOTCAR_provIndGrup_F.csv
		
		
		printlog "**  Ejecutando Proceso extraccion Parcial APR. BATCH...**"
		printlog "--------------------------------------------------------"	
		echo ""
		echo ""
		Mensaje "**  Ejecutando Proceso extraccion Parcial APR. BATCH... **"
		Mensaje "----------------------------------------------------------"
		
	elif [ $Ejec_Parcial = 'PROT' ]
		then
		Tipo_Ejecuccion=PROT
		arch_proc_extrac=Archivo_Procesos_Extract_APR_protesto_sem.txt
		Valida_Archivo $DirCfg/$arch_proc_extrac
		Valida_Archivo $DirDat/$arch_banca
		# 1- Valida Archivo Entrada detalletotcar_individuales.CSV: Detalle Provisiones Comerciales Individual
		Valida_Archivo $DirIn/$arch_prov_ind
		# 2- Valida Archivo Entrada DETALLETOTCAR_provIndGrup.CSV: Detalle Provisiones Comerciales Grupal 
		Valida_Archivo $DirIn/$arch_prov_gru
		
		# Formateo de Archivos, Elimina Encabezado de archivo y reemplaza "," por "."
		sed '1d' $DirIn/$arch_prov_ind | sed -e 's/[,]/./g' > $DirTmp/detalletotcar_individuales_F.csv
		sed '1d' $DirIn/$arch_prov_gru | sed -e 's/[,]/./g' > $DirTmp/DETALLETOTCAR_provIndGrup_F.csv


		printlog "**  Ejecutando Proceso extraccion Parcial APR. Protestos BCI, Informe Semanal...**"
		printlog "----------------------------------------------------------------------------------"
		echo ""
		echo ""
		Mensaje "** Ejecutando Proceso extraccion Parcial APR. Protestos BCI, Informe Semanal...**"
		Mensaje "---------------------------------------------------------------------------------"
	fi

fi	




#obtengo numero de lineas del archivo de Configuración
if test -s $DirCfg/$arch_proc_extrac
then
	N_Proc=`wc -l $DirCfg/$arch_proc_extrac | awk '{ printf "%i", $1 }'`
fi

echo ""


inicio=1
while [ $inicio -le $N_Proc ]; do

if [ $inicio -lt "10" ]
then
	indice=0$inicio
else
	indice=$inicio
fi

NombreProc=`awk '$1 ~ /'$indice'/ {print $2}' $DirCfg/$arch_proc_extrac`
TipoProc=`awk '$1 ~ /'$indice'/ {print $3}' $DirCfg/$arch_proc_extrac`
IndEjecProc=`awk '$1 ~ /'$indice'/ {print $4}' $DirCfg/$arch_proc_extrac`



#Ejecución de Modulos *.sql, Identificando si es (Bteq, Fload, Fexport). Tambien si el Indicador de Ejecución = 'S'
if [ $TipoProc = 'BTEQ' -a $IndEjecProc = 'S' ]
then
	printlog "Ejecutando Modulo $NombreProc"
	sh $DirShe/bteq.sh $NombreProc
	estado=$?
	#Validamos Ejecución del Modulo
	if [ $estado -ne 0 ]
	then
		printlog "Error en Modulo $NombreProc"
		exit 2
	fi
	printlog "Ok Ejecucion Correcta Modulo $NombreProc"
fi

if [ $TipoProc = 'FLOAD' -a $IndEjecProc = 'S' ]
then
	printlog "Ejecutando Modulo $NombreProc"
	sh $DirShe/fload.sh $NombreProc
	estado=$?
	# Validamos Ejecución del Modulo
	if [ $estado -ne 0 ]
	then
		printlog "Error en Modulo $NombreProc"
		exit 2
	fi
	printlog "Ok Ejecucion Correcta Modulo $NombreProc"
fi

if [ $TipoProc = 'FEXP' -a $IndEjecProc = 'S' ]
then
	printlog "Ejecutando Modulo $NombreProc"
	sh $DirShe/fexp.sh $NombreProc
	estado=$?
	# Validamos Ejecución del Modulo
	if [ $estado -ne 0 ]
	then
		printlog "Error en Modulo $NombreProc"
		exit 2
	fi
	printlog "Ok Ejecucion Correcta Modulo $NombreProc"
fi

 if [ $TipoProc = 'SAWK' -a $IndEjecProc = 'S' ]
then

	if [ ! -f $DirSql/${NombreProc}.awk ]
		then 
			printlog "Error en Modulo $NombreProc no existe"
			Mensaje "ERROR: Script $NombreProc no existe..."
			echo 'Error en Modulo '${NombreProc}' no existe' > $DirLog/${NombreProc}_LOG.txt
			Mensaje ""
			exit 2
		else
			echo " "
			printlog "Ejecutando Modulo $NombreProc"
			Mensaje "Ejecutando Script $NombreProc..."
			. $DirSql/${NombreProc}.awk > $DirLog/${NombreProc}_LOG.txt 2>&1
			estado=$?
			# Validamos Ejecución del Modulo
			Mensaje "Validando ejecucion ${NombreProc}..."
			if [ $estado -ne 0 ]
			then
				Mensaje "ERROR en AWK - Status final: ${estado}"
				printlog "Error en Modulo $NombreProc"
				exit 2
			fi
			printlog "Ok Ejecucion Correcta Modulo $NombreProc"
			Mensaje "OK en AWK - Status final: ${estado}"
			Mensaje ""
	fi
fi

let inicio=inicio+1
done

#elimina Archivos Temporales
Eliminar_Archivo $DirTmp/detalletotcar_individuales_F.csv
Eliminar_Archivo $DirTmp/DETALLETOTCAR_provIndGrup_F.csv


if [ $# -eq 1 ]
then
	mv $DirTmp/balan_ifrs_resp.txt $arch_balan_ifrs 
fi	

printlog ""
printlog ""
printlog "Archivos de salidas:"
Mensaje "**  Archivos de salidas  **"
Mensaje "---------------------------"

case $Tipo_Ejecuccion in
	COMPLE)
		Valida_Archivo $ARCH_CLIENTES
		Valida_Archivo $ARCH_DUE_BCI
		Valida_Archivo $ARCH_DEU_SBIF
		Valida_Archivo $ARCH_COM_CLI
		Valida_Archivo $ARCH_PROT_BCI
		Valida_Archivo $ARCH_PROT_SBIF
		Valida_Archivo $ARCH_BALANCE
		Valida_Archivo $ARCH_PRO_REN_COD_DESC
		printlog "...Archivos generados correctamente"
		Mensaje "...Archivos generados correctamente"
	
		printlog "Generando Archivo Control..."
		Mensaje ""
		Mensaje "**  Generando Archivos de Control  **"
		Mensaje "-------------------------------------"
		Num_Reg=`wc -l $ARCH_CLIENTES | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_Clientes}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_DUE_BCI | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_DeuBci}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_DEU_SBIF | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_DeuSbif}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_COM_CLI | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_Comp}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_PROT_BCI | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_ProtBci}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_PROT_SBIF | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_ProtSbif}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_BALANCE | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_Balace}_Control.ctr
		
		Num_Reg=`wc -l $ARCH_PRO_REN_COD_DESC | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_ProRen_Cod}_Control.ctr
		
		Mensaje "Archivo control: ${arch_Clientes}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_DeuBci}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_DeuSbif}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_Comp}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_ProtBci}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_ProtSbif}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_Balace}_Control.ctr OK"
		Mensaje "Archivo control: ${arch_ProRen_Cod}_Control.ctr OK"
		Mensaje "...Archivos  de contol generados correctamente"
		printlog "Archivo control: ${arch_Clientes}_Control.ctr OK"
		printlog "Archivo control: ${arch_DeuBci}_Control.ctr OK"
		printlog "Archivo control: ${arch_DeuSbif}_Control.ctr OK"
		printlog "Archivo control: ${arch_Comp}_Control.ctr OK"
		printlog "Archivo control: ${arch_ProtBci}_Control.ctr OK"
		printlog "Archivo control: ${arch_ProtSbif}_Control.ctr OK"
		printlog "Archivo control: ${arch_Balace}_Control.ctr OK"
		printlog "Archivo control: ${arch_ProRen_Cod}_Control.ctr OK"
		printlog "...Archivos de contol generados correctamente"
		
		#elimina Archivos Temporales
		Eliminar_Archivo $DirTmp/EPI_Estrategia_F.csv
		Eliminar_Archivo $DirTmp/0016BT0068S_T
		Eliminar_Archivo $DirTmp/0016BT021_T
		Eliminar_Archivo $DirTmp/0016BT028_T
		Eliminar_Archivo $DirTmp/${arch_prt_sfs}_t.txt

    ;; 	
	BATCH)
	    Valida_Archivo $ARCH_BATCH_28
		Valida_Archivo $ARCH_BATCH_68
		Valida_Archivo $ARCH_BATCH_21
		printlog "...Archivos generados correctamente"
		Mensaje "...Archivos generados correctamente"

		# Creando Registro de Control Batch 68
		Num_Reg=`wc -l $ARCH_BATCH_68 | awk '{ printf "%i", $1 }'`
		printf "RC""%06d%8s\n" $Num_Reg $fecha > $DirTmp/RC68
		cat $DirTmp/RC68 $ARCH_BATCH_68 > $DirOut/0016BT0068I${fec_ejec_proc}1
		Eliminar_Archivo $DirTmp/RC68
		
		####################################################################################################
		# Creando Registro de Control Batch 21
		Num_Reg=`wc -l $ARCH_BATCH_21 | awk '{ printf "%i", $1 }'`
		printf "%06d%8s\n" $Num_Reg $fecha > $DirTmp/RC21
		cat $DirTmp/RC21 $ARCH_BATCH_21 > $DirOut/0016BT021${fec_ejec_proc}01
		Eliminar_Archivo $DirTmp/RC21
		
		
		printlog "Generando Archivo Control..."
		Mensaje ""
		Mensaje "**  Generando Archivos de Control  **"
		Mensaje "-------------------------------------"
		Num_Reg=`wc -l $ARCH_BATCH_28 | awk '{ printf "%i", $1 }'`
		printf "RC""%06d%8s" $Num_Reg $fecha > $DirOut/0016BT028${fec_ejec_proc}01.ctr
		Num_Reg=`wc -l $ARCH_BATCH_68 | awk '{ printf "%i", $1 }'`
		printf "RC""%06d%8s" $Num_Reg $fecha > $DirOut/0016BT0068I${fec_ejec_proc}1.ctr
		Num_Reg=`wc -l $ARCH_BATCH_21 | awk '{ printf "%i", $1 }'`
		printf "%06d%8s\n" $Num_Reg $fecha > $DirOut/0016BT021${fec_ejec_proc}01.ctr
		Eliminar_Archivo $DirOut/0016BT0068I${fec_ejec_proc}N
		Eliminar_Archivo $DirOut/0016BT0021${fec_ejec_proc}N
		
		Mensaje "Archivo control: 0016BT028${fec_ejec_proc}01.ctr OK"
		Mensaje "Archivo control: 0016BT0068I${fec_ejec_proc}1.ctr OK"
		Mensaje "Archivo control: 0016BT021${fec_ejec_proc}01.ctr OK"
		Mensaje "...Archivos  de contol generados correctamente"
		printlog "Archivo control: 0016BT028${fec_ejec_proc}01.ctr OK"
		printlog "Archivo control: 0016BT0068I${fec_ejec_proc}1.ctr OK"
		printlog "Archivo control: 0016BT021${fec_ejec_proc}01.ctr OK"
		printlog "...Archivos de contol generados correctamente"
	
    ;;  
	PROT)
	    Valida_Archivo $ARCH_PROT_BCI_SEM
		echo "FECHA PROCESO;RUT CLIENTE;RAZON SOCIAL;CODIGO SUBBANCA;DESCRIPCION SUBBANCA;REGIONAL;CANTIDAD DE PROTESTO;MONTO DE PROTESTO;FECHA DE EJECUCION" > $DirTmp/encabezado.csv
		cat $DirTmp/encabezado.csv $ARCH_PROT_BCI_SEM > $DirtOut/${arch_ProtBci}_Semanal_${fec_ejec_proc}.csv
		printlog "...Archivo generado correctamente"
		Mensaje "...Archivo generado correctamente"
		
		printlog "Generando Archivo Control..."
		Mensaje ""
		Mensaje "**  Generando Archivos de Control  **"
		Mensaje "-------------------------------------"
		Num_Reg=`wc -l $ARCH_PROT_BCI_SEM | awk '{ printf "%i", $1 }'`
		printf "%8s%010d%8s" $FecPrc $Num_Reg $fecha > $DirtOut/${arch_ProtBci}_Control.ctr
		
		Mensaje "Archivo control: ${arch_ProtBci}_Control.ctr OK"
		Mensaje "...Archivos  de contol generados correctamente"
		printlog "Archivo control: ${arch_ProtBci}_Control.ctr OK"
		printlog "...Archivos de contol generados correctamente"
		#elimina Archivos Temporales
		Eliminar_Archivo $ARCH_PROT_BCI_SEM
		Eliminar_Archivo $DirTmp/encabezado.csv
    ;; 	 
esac 
		


printlog ""
printlog "$fec_ejec_proc.$Hora"
printlog "FIN shell $NOMBRE_SHELL"
printlog " "
Mensaje ""
Mensaje "FIN shell $NOMBRE_SHELL"

exit 0
