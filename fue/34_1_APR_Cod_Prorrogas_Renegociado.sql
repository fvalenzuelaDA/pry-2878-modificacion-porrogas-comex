.logon usuario,password;

SEL DATE, TIME;


/* **************************************************************************************************** */
/*Nombre Bteq				:34_1_APR_Cod_Prorrogas_Renegociado.sql										*/
/*Nombre BD					:EDW_VW																*/
/*Tipo de ejecucion			:Batch																		*/
/*Fecha de creacion			:13/12/2021																	*/
/*Autor						:ADA LTDA																	*/
/*Objetivos					:Se extrae la informacion de codigos y descripciones que identificab a las	*/  
/*								operaciones prorrogadas													*/
/*Canal de ejecucion		:Control-M																	*/
/*Parametros de entrada		:${BD_VW}																	*/
/*							 ${BD_TEMP}																	*/
/*Tabla de entrada			:${BD_TEMP}.APR_CLI_REN_PRO_S												*/
/*Tabla de salida			:${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S 										*/
/*Retorno					:Codigos y descripciones de Operaciones prorrogadas y renegociadas			*/
/*Ejemplo de ejecucion		:No aplica, ya que es ejecutado por medio de una shell						*/
/* **************************************************************************************************** */



/* ************************************************************************************* */
/* TABLA TEMPORAL CON OPERACIONES PRORROGADAS Y RENEGOCIADAS CON SUS TIO-AUX			*/
/* ************************************************************************************* */
DROP TABLE ${BD_TEMP}.APR_CLI_REN_PRO_COD;
CREATE SET TABLE ${BD_TEMP}.APR_CLI_REN_PRO_COD	(
FEC_PROC DATE FORMAT 'YY/MM/DD'
,ACCOUNT_NUM		VARCHAR(18)
,ACCOUNT_MODIFIER_NUM	VARCHAR(18)
,ACCOUNT_TYPE_CD	VARCHAR(06)
) PRIMARY INDEX (ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM);

.IF ERRORCODE<>0 THEN .QUIT 8;


--SE INSERTAS TIO-AUX 
INSERT INTO ${BD_TEMP}.APR_CLI_REN_PRO_COD
SELECT
	A.FEC_PROC
	,A.ACCOUNT_NUM
	,A.ACCOUNT_MODIFIER_NUM
	,B.ACCOUNT_TYPE_CD
FROM	
	${BD_TEMP}.APR_CLI_REN_PRO_S A
	,${BD_VW}.AGREEMENT B
WHERE
	A.ACCOUNT_NUM = B.ACCOUNT_NUM
AND A.ACCOUNT_MODIFIER_NUM = B.ACCOUNT_MODIFIER_NUM
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX (  ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM)
ON ${BD_TEMP}.APR_CLI_REN_PRO_COD;
.IF ERRORCODE<>0 THEN .QUIT 8;


/* ************************************************************************************* */
/* TABLA TEMPORAL CON OPERACIONES PRORROGADAS Y RENEGOCIADAS CON SUS TIO-AUX Y DESC			*/
/* ************************************************************************************* */
DROP TABLE ${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S;
CREATE SET TABLE ${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S (
	FEC_PROC 				DATE FORMAT 'YY/MM/DD'
	,ACCOUNT_NUM				VARCHAR(18)
	,ACCOUNT_MODIFIER_NUM	VARCHAR(18)
	,ACCOUNT_TYPE_CD		VARCHAR(06)	
	,TAB_GLS_DESC			VARCHAR(255)
) PRIMARY INDEX (ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM);

.IF ERRORCODE<>0 THEN .QUIT 8;


--INSERTA DESCRIPCIONES DE TIO-AUX
INSERT INTO ${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S
SELECT
	FEC_PROC
	,ACCOUNT_NUM				
	,ACCOUNT_MODIFIER_NUM			
	,ACCOUNT_TYPE_CD		
	,TAB_GLS_DESC			
FROM
	${BD_TEMP}.APR_CLI_REN_PRO_COD A
	,${BD_VW}.TAB	B	
WHERE		
	A.ACCOUNT_TYPE_CD = B.TAB_COD_CTAB
AND TAB_ID = 150
AND	TAB_COD_TTAB = 'AUX'		
;

.IF ERRORCODE<>0 THEN .QUIT 8;

COLLECT STATISTICS USING SAMPLE
INDEX (  ACCOUNT_NUM, ACCOUNT_MODIFIER_NUM)
ON ${BD_TEMP}.APR_CLI_REN_PRO_COD_DESC_S;
.IF ERRORCODE<>0 THEN .QUIT 8;


SEL DATE, TIME;

.LOGOFF;

.QUIT 0;
